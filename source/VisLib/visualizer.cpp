#include <visualizer.h>

void drawFrame(Eigen::Matrix4f &frame, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer)
{
	//this draws a cone to visualize our point in space. ndi software can give us values for point locations and normals. once we have values for normals, I can make the cone tip point in the normal direction 

	pcl::ModelCoefficients xAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients yAxis = pcl::ModelCoefficients();
	pcl::ModelCoefficients zAxis = pcl::ModelCoefficients();

	//x,y,z,dx,dy,dz,angle
	xAxis.values = std::vector<float>{ 0, 0, 0, 1, 0, 0, 10 };
	yAxis.values = std::vector<float>{ 0, 0, 0, 0, 1, 0, 10 };
	zAxis.values = std::vector<float>{ 0, 0, 0, 0, 0, 1, 10 };

	std::string xName = name + "_x";
	if (!visualizer->contains(xName))
	{
		visualizer->addCone(xAxis, xName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0.5, 0, 0, xName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, xName);
	}
	visualizer->updateShapePose(xName, Eigen::Affine3f(frame));

	std::string yName = name + "_y";
	if (!visualizer->contains(yName))
	{
		visualizer->addCone(yAxis, yName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0.5, 0, yName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, yName);
	}
	visualizer->updateShapePose(yName, Eigen::Affine3f(frame));

	std::string zName = name + "_z";
	if (!visualizer->contains(zName))
	{
		visualizer->addCone(zAxis, zName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0.5, zName);
		visualizer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_SHADING, pcl::visualization::PCL_VISUALIZER_SHADING_GOURAUD, zName);
	}
	visualizer->updateShapePose(zName, Eigen::Affine3f(frame));

}

void translate(Eigen::Matrix4f &inframe, std::vector<std::string> valuesFromFile, int line, int NumofSensors, int frameNum, std::vector<double> &coordVec)
{
	double x = coordVec[3 * frameNum];
	double y = coordVec[3 * frameNum + 1];
	double z = coordVec[3 * frameNum + 2];

	//initialize cone at center
	double lineNum, xnew = 0, ynew = 0, znew = 0;
	int j, k, l, m;

	j = (1 + NumofSensors * 3)* line;
	k = 1 + (3 * frameNum) + (1 + NumofSensors * 3)* line;
	l = 2 + (3 * frameNum) + (1 + NumofSensors * 3)* line;
	m = 3 + (3 * frameNum) + (1 + NumofSensors * 3)* line;

	//cout << " " << endl;
	//cout << " " << endl;
	//cout << frameNum << endl;
	//cout << line << endl;

	//use data from text file to create transformation matrix, define xnew ynew znew and translation values
	std::vector<std::string> coords = valuesFromFile;

	if (line == 0)
	{
		x = 0;
		y = 0;
		z = 0;
	}

	if (coords[k] == " " && coords[l] == " " && coords[m] == " ")
	{
		lineNum = stod(coords[j]);
		xnew = 0;
		ynew = 0;
		znew = 0;
	}
	else
	{
		lineNum = stod(coords[j]);
		xnew = stod(coords[k]) / 10;
		ynew = stod(coords[l]) / 10;
		znew = stod(coords[m]) / 10;
	}

	double xtrans = xnew - x;
	double ytrans = ynew - y;
	double ztrans = znew - z;

	//cout << j << " " << k << " " << l << " " << m << endl;
	//cout << xnew << ynew << znew << endl;
	//cout << x << y << z << endl;
	//cout << xtrans << ytrans << ztrans << endl;

	inframe(0, 3) = x;
	inframe(1, 3) = y;
	inframe(2, 3) = z;


	//set x y z to new values for next translation
	coordVec[3 * frameNum] = xnew;
	coordVec[3 * frameNum + 1] = ynew;
	coordVec[3 * frameNum + 2] = znew;
	//cout << x << y << z << endl;

}

std::vector<std::string> getdata(std::string filename)
{
	using namespace std;
	fstream dataFile;
	dataFile.open(filename, ios::in);
	vector<string> data;
	int j = 0;

	if (!dataFile)
	{
		cout << "Error opening file." << endl;
		exit(1);
	}
	else
	{
		char FullDoc[100000];
		//Reads the file if it's open
		while (dataFile.good())
		{
			dataFile.read(FullDoc, 100000);
		}

		data = split(FullDoc, ',');
		return data;

	}
	dataFile.close();
}

std::vector<std::string> split(const std::string &s, char delim)
{
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> tokens;
	//Splits strings by a given delimiter
	while (getline(ss, item, delim)) {
		tokens.push_back(item);
	}
	return tokens;
}