#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <boost/thread/thread.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common_headers.h>
#include <pcl\PolygonMesh.h>

void drawFrame(Eigen::Matrix4f &frame, std::string name, boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizer);
void translate(Eigen::Matrix4f &inframe, std::vector<std::string> valuesFromFile, int line, int NumofSensors, int frameNum, std::vector<double> &coordVec);
std::vector<std::string> getdata(std::string filename);
std::vector<std::string> split(const std::string &s, char delim);

