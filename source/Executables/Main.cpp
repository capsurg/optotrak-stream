#include "sockets.h"
#include "Run_Camera.h"
#include "visualizer.h"

extern "C" {
#include "ot_aux.h"
}

#define NUM_RIGID_BODIES    1
const int numMarkers = 3;

float frequency = 500.0;	//desired camera freuency
int frameCount = 0;

typedef struct RigidBodyDataStruct
{
	struct OptotrakRigidStruct  pRigidData[NUM_RIGID_BODIES];
	Position3d                  p3dData[numMarkers];
} RigidBodyDataType;

void main(int argc, const char *argv[])
{
	//Define local variables
	unsigned int
		uFlags,
		uElements,
		uFrameCnt,
		uRigidCnt;
	RigidBodyDataType
		RigidBodyData;
	Opto optotrak;
	std::string
		IP,
		outMessage,
		inMessage;
	BoostSoc client;
	BoostSoc server;
	int port;

	//Initialize the system, open socket communication and connect, get ready to request data
	optotrak.initialize(argc, argv, numMarkers, frequency);
	client.clientConnect(2003, "127.0.0.1", false, -1);
	while (!client.getConnected())
	{
		std::cout << "awaiting serial connection" << std::endl;
		std::cout << "client: " << client.getStatus() << std::endl;
		Sleep(1000);
	}

	std::cout << "Client connected./n";
	
	//This program is designed to wait until it recieves a start message from the server, so it will sit in this while loop unti
	inMessage == "start";

	optotrak.ActivateMarkers();
	for (int i = 0; i < NUM_RIGID_BODIES; i++)
	{
		optotrak.initRigidBody(i);
	}

	//If using more than one rigid body, and you want to use the first rigid body as frame of reference for the second,
	//uncomment the code below. This is helpful for quickly changing the "origin", if you want to change multiple times
	//without making a new camera parameter file every time

	//if (RigidBodyChangeFOR(RIGID_BODY_1, OPTOTRAK_CONSTANT_RIGID_FLAG))
	//{
	//	optotrak.Error_Exit();
	//}
	outMessage = "";
	while (inMessage != "stop")
	{
		
		//Check for errors in system before getting the data
		if (DataGetLatestTransforms(&uFrameCnt, &uElements, &uFlags,
			&RigidBodyData))
		{
			optotrak.Error_Exit();
		}
		
		if (RigidBodyData.pRigidData->flags & OPTOTRAK_UNDETERMINED_FLAG)
		{
			std::cout << "Undetermined transform!" << std::endl;
			outMessage = "Undetermined Transform! \n";
			if (RigidBodyData.pRigidData->flags & OPTOTRAK_RIGID_ERR_MKR_SPREAD)
			{
				std::cout << "Marker spread error." << std::endl;
			}
			continue;
		}

		
		//Print out system data: Frame number, timestamp, X, Y, Z translation data, rotation matrix data
		for (int i = 0; i < NUM_RIGID_BODIES; i++)
		{
			outMessage = std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.translation.x) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.translation.y) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.translation.z) + ","
			    + std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[0][0]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[0][1]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[0][2]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[1][0]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[1][1]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[1][2]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[2][0]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[2][1]) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.matrix[2][2]);
			//std::cout << frameCount << std::endl;
			std::cout << outMessage << std::endl;
		}
		
		client.sendMessage(outMessage);
		inMessage = client.getMessage();
		frameCount++;
	}
	
	optotrak.NormExit();
}
