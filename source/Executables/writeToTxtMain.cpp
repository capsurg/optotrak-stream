#include "Run_Camera.h"
#include <iostream>
#include <fstream>

extern "C" {
#include "ot_aux.h"
}

#define NUM_RIGID_BODIES    1
int RIGID_BODY_1 = 0;
const int numMarkers = 3;

float frequency = 500.0;	//desired camera freuency
int numFrames = 50;		//desired number of frames of data
int frameCount = 0;

//https://www.youtube.com/watch?v=JNktqnwhpPA use this for USB TCP Socket stuff
 
typedef struct RigidBodyDataStruct
{
	struct OptotrakRigidStruct  pRigidData[NUM_RIGID_BODIES];
	Position3d                  p3dData[numMarkers];
} RigidBodyDataType;

void main(int argc, const char *argv[])
{
	//Define local variables
	unsigned int
		uFlags,
		uElements,
		uFrameCnt,
		uRigidCnt;
	RigidBodyDataType
		RigidBodyData;
	Opto optotrak;
	std::ofstream file;
	std::string outMessage;

	//Initialize the system, open socket communication and connect, get ready to request data
	optotrak.initialize(argc, argv, numMarkers, frequency);
	optotrak.ActivateMarkers();
	file.open("C:\\Users\\cew\\Desktop\\OutputValues.txt");
	Sleep(1000);

	//If using more than one rigid body, and you want to use the first rigid body as frame of reference for the second,
	//uncomment the code below. This is helpful for quickly changing the "origin", if you want to change multiple times
	//without making a new camera parameter file every time

	//cout << "...RigidBodyChangeFOR" << endl;
	//if (RigidBodyChangeFOR(RIGID_BODY_1, OPTOTRAK_CONSTANT_RIGID_FLAG))
	//{
	//	optotrak.Error_Exit();
	//}

	//Request data from camera n times, where n = desired number of data frames
	for(frameCount; frameCount < numFrames; frameCount++)
	{
		outMessage = "";
		//Check for errors in system before getting the data
		if (DataGetLatestTransforms(&uFrameCnt, &uElements, &uFlags,
			&RigidBodyData))
		{
			optotrak.Error_Exit();
		}
		if (RigidBodyData.pRigidData->flags & OPTOTRAK_UNDETERMINED_FLAG)
		{
			outMessage = "Undetermined transform!";
			std::cout << outMessage << std::endl;
			file << outMessage << std::endl;
			if (RigidBodyData.pRigidData->flags & OPTOTRAK_RIGID_ERR_MKR_SPREAD)
			{
				std::cout << "Marker spread error." << std::endl;
			}
			continue;
		}

		std::string timestamp = optotrak.getTimestamp();

		//Print out system data: Frame number, timestamp, X, Y, Z translation data, rotation matrix data
		for (int i = 0; i < NUM_RIGID_BODIES; i++)
		{
			outMessage = std::to_string(frameCount) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.translation.x) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.translation.y) + ","
				+ std::to_string(RigidBodyData.pRigidData[i].transformation.rotation.translation.z) + ",";
;
			std::cout << outMessage << std::endl;
			file << outMessage << std::endl;
		}

	}
	file.close();
	optotrak.NormExit();
}
