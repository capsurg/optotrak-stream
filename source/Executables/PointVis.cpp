#include <visualizer.h>

int main()
{
	std::string filename;
	std::cout << "What file would you like to use?" << endl;
	std::cin >> filename;

	int sensorNum;
	std::cout << "How many sensors are connected?" << endl;
	std::cin >> sensorNum;

	int numOfFrames;
	std::cout << "How any frame values are there?" << endl;
	std::cin >> numOfFrames;

	boost::shared_ptr<pcl::visualization::PCLVisualizer> Vis(new pcl::visualization::PCLVisualizer("Camera View"));
	Vis->setBackgroundColor(0, 0, 0);
	Vis->addCoordinateSystem(10.0);
	Vis->initCameraParameters();

	std::vector<std::string> data = getdata(filename);

	while (!Vis->wasStopped())
	{
		std::vector<double> xyz(sensorNum * 3);
		std::vector<Eigen::Matrix4f> allFrames(sensorNum);
		for (int i(0); i < sensorNum; i++)
		{
			allFrames[i] = Eigen::Matrix4f::Identity();
		}

		for (int counter(0); counter < numOfFrames; counter++)
		{
			for (int i(0); i < sensorNum; i++)
			{
				Eigen::Matrix4f frame = allFrames[i];

				translate(frame, data, counter, sensorNum, i, xyz);
				drawFrame(frame, "frame" + std::to_string(i), Vis);

				Vis->spinOnce(40);
			}
		}
	}

}