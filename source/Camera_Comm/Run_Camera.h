#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <chrono>
#include <ctime>
#include <Windows.h>
#include <iostream>

#include "ndtypes.h"
#include "ndpack.h"
#include "ndopto.h"

class Opto
{
public:
	void Error_Exit();

	std::string getTimestamp();

	void initialize(int argc, const char *argv[], const int NumMarker, float frequency);

	void initRigidBody(int rigidBodyNum);

	void ActivateMarkers();

	void NormExit();

	std::string MyDisplayMarker(int nMarker, Position3d dtPosition3d);
};

