/*****************************************************************
Name:             ProCMM_Get3D.C

Description:
 	Optotrak ProCMM OAPI Sample - Get 3D Data

*****************************************************************/

/*****************************************************************
C Library Files Included
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
void sleep( unsigned int uSec );
#else
#include <unistd.h>
#endif

/*****************************************************************
ND Library Files Included
*****************************************************************/
#include "ndtypes.h"
#include "ndpack.h"
#include "ndopto.h"

/*
 * This are sample frequencies.
 * Depending on the number of markers, the MarkerFrequency may need to be increased.
 * If MarkerFrequency exceeds 4500, the FrameFrequency may need to be decreased
 */
#define SAMPLE_MARKERFREQ		4000.0f
#define SAMPLE_FRAMEFREQ		50.0f
#define SAMPLE_DUTYCYCLE		0.65f
#define SAMPLE_VOLTAGE			15.0f
#define SAMPLE_STREAMDATA		0

#define PROCMM_MARKER_TYPE		7
#define PROCMM_WAVELENGTH_TYPE	1
#define PROCMM_MODEL_TYPE		6


/*****************************************************************
Application Files Included
*****************************************************************/
#include "certus_aux.h"
#include "ot_aux.h"


void main( int argc, char *argv[] )
{
	OptotrakSettings dtSettings;
    char szNDErrorString[MAX_ERROR_STRING_LENGTH + 1];
    int i;
	int nCurDevice;
	int nCurFrame;
	int nDevices;
	int nDeviceMarkers;
	int nRigidBodies;
	int nMarkersToActivate;
    int nMarkerType;
	int nWavelengthType;
	int nModelType;
	ApplicationDeviceInformation *pdtDevices;
    DeviceHandle *pdtDeviceHandles;
    DeviceHandleInfo *pdtDeviceHandlesInfo;
    unsigned int uFlags;
    unsigned int uElements;
    unsigned int uFrameNumber;
	Position3d *pData3d;
    void *pDataRaw;
    int rb;
	
    /*
     * initialization
     */
	pdtDevices = NULL;
	pdtDeviceHandles = NULL;
	pdtDeviceHandlesInfo = NULL;
	pData3d = NULL;
    pDataRaw = NULL;
	nMarkersToActivate = 0;
	nDevices = 0;
	nDeviceMarkers = 0;
	nRigidBodies = 0;
	dtSettings.nMarkers = 0;
	dtSettings.fFrameFrequency = SAMPLE_FRAMEFREQ;
	dtSettings.fMarkerFrequency = SAMPLE_MARKERFREQ;
	dtSettings.nThreshold = 5;
	dtSettings.nMinimumGain = 160;
	dtSettings.nStreamData = SAMPLE_STREAMDATA;
	dtSettings.fDutyCycle = SAMPLE_DUTYCYCLE;
	dtSettings.fVoltage = SAMPLE_VOLTAGE;
	dtSettings.fCollectionTime = 1.0;
	dtSettings.fPreTriggerTime = 0.0;

	/*
	 * Announce that the program has started
	 */
	fprintf( stdout, "\nOptotrak PROcmm sample program - Get3D\n\n" );

	/*
	 * look for the -nodld parameter that indicates 'no download'
	 */
	if( ( argc < 2 ) || ( strncmp( argv[1], "-nodld", 6 ) != 0 ) )
	{
		/*
		 * Load the system of processors.
		 */
		fprintf( stdout, "...TransputerLoadSystem\n" );
		if( TransputerLoadSystem( "system" ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */

		sleep( 1 );
	} /* if */

    /*
     * Communication Initialization
     * Once the system processors have been loaded, the application
     * prepares for communication by initializing the system processors.
     */
	fprintf( stdout, "...TransputerInitializeSystem\n" );
    if( TransputerInitializeSystem( OPTO_LOG_ERRORS_FLAG | OPTO_LOG_STATUS_FLAG ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Determine if this sample will run with the system attached.
	 * This sample is intended for Optotrak Certus systems.
	 */
	fprintf( stdout, "...DetermineSystem\n" );
	if( uDetermineSystem( ) != OPTOTRAK_CERTUS_FLAG )
	{
		goto PROGRAM_COMPLETE;
	} /* if */

    /*
     * Strober Initialization
     * Once communication has been initialized, the application must
     * determine the strober configuration.
     * The application retrieves device handles and all strober
     * properties from the system.
     */
	fprintf( stdout, "...DetermineStroberConfiguration\n" );
	if( DetermineStroberConfiguration( &pdtDeviceHandles, &pdtDeviceHandlesInfo, &nDevices ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

    /*
     * Now that all the device handles have been completely set up,
     * the application can store all the device handle information in
     * an internal data structure.  This will facilitate lookups when
     * a property setting needs to be checked.
     */
    ApplicationStoreDeviceProperties( &pdtDevices, pdtDeviceHandlesInfo, nDevices );

	/*
	 * Add rigid body data from device handle
	 */
	for( nCurDevice = 0; nCurDevice < nDevices; nCurDevice++ )
	{
		/*
		 * if the device has a ROM, use the rigid body from the rom to set up the collection
		 */
		if( pdtDevices[nCurDevice].bHasROM )
		{
			fprintf( stdout, "...RigidBodyAddFromDeviceHandle for %s\n", pdtDevices[nCurDevice].szName );
			if( RigidBodyAddFromDeviceHandle( pdtDeviceHandlesInfo[nCurDevice].pdtHandle->nID, nRigidBodies, OPTOTRAK_RETURN_QUATERN_FLAG | OPTOTRAK_QUATERN_RIGID_FLAG ) != OPTO_NO_ERROR_CODE )
			{
				goto ERROR_EXIT;
			} /* if */

			nRigidBodies++;
		}
		else
		{
			/* this device has no SROM and should not be activated */
		} /* for */
	} /* for */
	fprintf( stdout, "\n" );

    RigidBodyChangeFOR( -1, OPTOTRAK_CONSTANT_RIGID_FLAG );

	/*
     * Retrieve the device properties again to verify that the changes took effect.
     */
	for( nCurDevice = 0; nCurDevice < nDevices; nCurDevice++ )
	{
		if( GetDevicePropertiesFromSystem( &(pdtDeviceHandlesInfo[nCurDevice]) ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */
	} /* for */

	if( ApplicationStoreDeviceProperties( &pdtDevices, pdtDeviceHandlesInfo, nDevices ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Retrieve ProCMM Data
	 */
	fprintf( stdout, "\n" );

	/*
	 * check if any devices have been detected by the system
	 */
	if( nDevices == 0 )
	{
		fprintf( stdout, ".........no devices detected.\n" );
		goto PROGRAM_COMPLETE;
	} /* if */

	/*
	 * Determine the collection settings based on the device properties
	 */
	ApplicationDetermineCollectionParameters( nDevices, pdtDevices, &dtSettings );

	/*
	 * Use PROcmm camera parameters
	 * PROcmm cameras use a specific set of marker, wavelength and model
	 */
    nMarkerType     = PROCMM_MARKER_TYPE;
    nWavelengthType = PROCMM_WAVELENGTH_TYPE;
    nModelType      = PROCMM_MODEL_TYPE;
    if( OptotrakSetCameraParameters( nMarkerType, nWavelengthType, nModelType ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
	 * Load camera parameters.
     */
	fprintf( stdout, "...OptotrakLoadCameraParameters\n" );
    if( OptotrakLoadCameraParameters( "standard" ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Load AUTOscale parameters
	 * Setting the input parameter to 'NULL' will load the latest AUTOscale parameters programmed on the tracker
	 * To load different AUTOscale parameters, specify the path to the .nas file into this function.
	 */
	fprintf( stdout, "...OptotrakLoadAutoScale\n" );
	if( OptotrakLoadAutoScale( NULL ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Ensure that we are firing some markers
	 */
	if( dtSettings.nMarkers == 0 )
	{
		fprintf( stdout, "Error: There are no markers to be activated.\n" );
		goto ERROR_EXIT;
	} /* if */

	/*
	 * allocate memory for 3d data
	 */
	pData3d = (Position3d*)malloc( dtSettings.nMarkers * sizeof( Position3d ) );
    pDataRaw = malloc( dtSettings.nMarkers * 4 /* sensors */ * 2 /* centroid + status */ * sizeof( float ) );

    /*
     * Configure Optotrak Collection
     * Once the system strobers have been enabled, and all settings are
     * loaded, the application can set up the Optotrak collection
     */
	fprintf( stdout, "...OptotrakSetupCollection\n" );
	fprintf( stdout, ".....%d, %.2f, %.0f, %d, %d, %d, %.2f, %.2f, %.0f, %.0f\n",
								 dtSettings.nMarkers,
			                     dtSettings.fFrameFrequency,
				                 dtSettings.fMarkerFrequency,
					             dtSettings.nThreshold,
						         dtSettings.nMinimumGain,
							     dtSettings.nStreamData,
								 dtSettings.fDutyCycle,
								 dtSettings.fVoltage,
								 dtSettings.fCollectionTime,
								 dtSettings.fPreTriggerTime );
    if( OptotrakSetupCollection( dtSettings.nMarkers,
			                     dtSettings.fFrameFrequency,
				                 dtSettings.fMarkerFrequency,
					             dtSettings.nThreshold,
						         dtSettings.nMinimumGain,
							     dtSettings.nStreamData,
								 dtSettings.fDutyCycle,
								 dtSettings.fVoltage,
								 dtSettings.fCollectionTime,
								 dtSettings.fPreTriggerTime,
								 OPTOTRAK_FULL_DATA_FLAG | OPTOTRAK_BUFFER_RAW_FLAG | OPTOTRAK_NO_FIRE_MARKERS_FLAG | OPTOTRAK_GET_NEXT_FRAME_FLAG | OPTOTRAK_SWITCH_AND_CONFIG_FLAG ) != OPTO_NO_ERROR_CODE )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Wait one second to let the camera adjust.
     */
    sleep( 1 );

    /*
     * Prepare for realtime data retrieval.
     * Activate markers. Turn on the markers prior to data retrieval.
     */
	fprintf( stdout, "...OptotrakActivateMarkers\n" );
    if( OptotrakActivateMarkers( ) != OPTO_NO_ERROR_CODE )
    {
        goto ERROR_EXIT;
    } /* if */
	sleep( 1 );

    /*
     * Get and display five frames of 3D data.
     */
    fprintf( stdout, "\n\nSample Program Results:\n\n" );
    for( nCurFrame = 0; nCurFrame < 10; nCurFrame++ )
    {
        /*
         * Get a frame of data.
         */
        fprintf( stdout, "\n" );
		if( DataGetNext3D( &uFrameNumber, &uElements, &uFlags, pData3d ) )
		{
			goto ERROR_EXIT;
		} /* if */

		/*
		 * Print out the valid data.
		 */
		fprintf( stdout, "\n" );
		fprintf( stdout, "Frame     : %8u\n", uFrameNumber );

		for( i = 0; i < dtSettings.nMarkers; i++ )
		{
			DisplayMarker( i, pData3d[i] );
		} /* for */
		fprintf( stdout, "\n" );

    } /* for */
    fprintf( stdout, "\n\n" );

    /*
     * De-activate the markers.
     */
	fprintf( stdout, "...OptotrakDeActivateMarkers\n" );
    if( OptotrakDeActivateMarkers() )
    {
        goto ERROR_EXIT;
    } /* if */

    // remove all previous rigid bodies
    for( rb = 0; rb < nRigidBodies; rb++ )
    {
        RigidBodyDelete( rb );
    }

    // add a rigid body based on the 3D positions
    nRigidBodies = 0;
    RigidBodyAdd( nRigidBodies, 1, dtSettings.nMarkers, (float*)pData3d, NULL, OPTOTRAK_QUATERN_RIGID_FLAG );

    // change rigid body settings
    RigidBodyChangeSettings( nRigidBodies, 3, 180, 0.4f, 1.0f, 0.4f, 0.2f, OPTOTRAK_QUATERN_RIGID_FLAG | OPTOTRAK_RETURN_QUATERN_FLAG );
    nRigidBodies++;

    // rigid body add from device handle
	for( nCurDevice = 0; nCurDevice < nDevices; nCurDevice++ )
	{
		/*
		 * if the device has a ROM, use the rigid body from the rom to set up the collection
		 */
		if( pdtDevices[nCurDevice].bHasROM )
		{
			fprintf( stdout, "...RigidBodyAddFromDeviceHandle for %s\n", pdtDevices[nCurDevice].szName );
			if( RigidBodyAddFromDeviceHandle( pdtDeviceHandlesInfo[nCurDevice].pdtHandle->nID, nRigidBodies, OPTOTRAK_RETURN_QUATERN_FLAG | OPTOTRAK_QUATERN_RIGID_FLAG ) != OPTO_NO_ERROR_CODE )
			{
				goto ERROR_EXIT;
			} /* if */

			nRigidBodies++;
		}
		else
		{
			/* this device has no SROM and should not be activated */
		} /* for */
	} /* for */

    /*
     * Wait one second to let the camera adjust.
     */
    sleep( 1 );

    /*
     * Prepare for realtime data retrieval.
     * Activate markers. Turn on the markers prior to data retrieval.
     */
	fprintf( stdout, "...OptotrakActivateMarkers\n" );
    if( OptotrakActivateMarkers( ) != OPTO_NO_ERROR_CODE )
    {
        goto ERROR_EXIT;
    } /* if */
	sleep( 1 );

    /*
     * Get and display five frames of 3D data.
     */
    fprintf( stdout, "\n\nSample Program Results:\n\n" );
    for( nCurFrame = 0; nCurFrame < 10; nCurFrame++ )
    {
        /*
         * Get a frame of data.
         */
        fprintf( stdout, "\n" );
        if( DataGetNextRaw( &uFrameNumber, &uElements, &uFlags, pDataRaw ) )
		{
			goto ERROR_EXIT;
		} /* if */

		if( OptotrakConvertFullRawTo3D( &uElements, pDataRaw, pData3d ) )
		{
			goto ERROR_EXIT;
		} /* if */

		/*
		 * Print out the valid data.
		 */
		fprintf( stdout, "\n" );
		fprintf( stdout, "Frame     : %8u\n", uFrameNumber );

		for( i = 0; i < dtSettings.nMarkers; i++ )
		{
			DisplayMarker( i, pData3d[i] );
		} /* for */
		fprintf( stdout, "\n" );

    } /* for */
    fprintf( stdout, "\n\n" );

	/*
	 * Stop the collection.
	 */
	fprintf( stdout, "...OptotrakStopCollection\n" );
	if( OptotrakStopCollection( ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

    /*
     * De-activate the markers.
     */
	fprintf( stdout, "...OptotrakDeActivateMarkers\n" );
    if( OptotrakDeActivateMarkers() )
    {
        goto ERROR_EXIT;
    } /* if */


PROGRAM_COMPLETE:
    /*
     * CLEANUP
     */
	fprintf( stdout, "\n" );
	fprintf( stdout, "...TransputerShutdownSystem\n" );
    TransputerShutdownSystem( );

	/*
	 * free all memory
	 */
	if( pdtDeviceHandlesInfo )
	{
		for( i = 0; i < nDevices; i++ )
		{
			AllocateMemoryDeviceHandleProperties( &(pdtDeviceHandlesInfo[i].grProperties), 0 );
		} /* for */
	} /* if */
	AllocateMemoryDeviceHandles( &pdtDeviceHandles, 0 );
	AllocateMemoryDeviceHandlesInfo( &pdtDeviceHandlesInfo, pdtDeviceHandles, 0 );
	free( pData3d );

	exit( 0 );


ERROR_EXIT:
	/*
	 * Indicate that an error has occurred
	 */
	fprintf( stdout, "\nAn error has occurred during execution of the program.\n" );
    if( OptotrakGetErrorString( szNDErrorString, MAX_ERROR_STRING_LENGTH + 1 ) == 0 )
    {
        fprintf( stdout, szNDErrorString );
    } /* if */

	fprintf( stdout, "\n\n...TransputerShutdownSystem\n" );
	OptotrakDeActivateMarkers( );
	TransputerShutdownSystem( );

	/*
	 * free all memory
	 */
	if( pdtDeviceHandlesInfo )
	{
		for( i = 0; i < nDevices; i++ )
		{
			AllocateMemoryDeviceHandleProperties( &(pdtDeviceHandlesInfo[i].grProperties), 0 );
		} /* for */
	} /* if */
	AllocateMemoryDeviceHandles( &pdtDeviceHandles, 0 );
	AllocateMemoryDeviceHandlesInfo( &pdtDeviceHandlesInfo, pdtDeviceHandles, 0 );
	free( pData3d );
    free( pDataRaw );

    exit( 1 );

} /* main */

