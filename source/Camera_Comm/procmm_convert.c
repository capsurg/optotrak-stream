/*****************************************************************
Name:             ProCMM_CONVERT.C

Description:
 	Optotrak ProCMM OAPI Sample - Convert Raw To 6D

*****************************************************************/

/*****************************************************************
C Library Files Included
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>

#ifdef _MSC_VER
#include <string.h>
#endif

/*****************************************************************
ND Library Files Included
*****************************************************************/
#include "ndtypes.h"
#include "ndopto.h"

void ShowUsage()
{
	fprintf( stdout, "\nProCMM_Convert FullRawFile RigidBodyFile CamFile Output6DFile [-3Output3DFile] [-aNASfile]\n" );
}

int main( int argc, char *argv[] )
{
	int i;
	char *rawFile = NULL;
	char *rigFile = NULL;
	char *camFile = NULL;
	char *nasFile = NULL;
	char *tipFile = NULL;
	char *outFile3D = NULL;
	char *outFile6D = NULL;
	int rawFileID = 0;
	int outFile6DID = 1;
	int outFile3DID = 2;
	int markerType = 2;
	int wavelength = 1;
	int modelType = 6;
	int numMarkers;
	int floatSubitems;
	int charSubitems;
	int intSubitems;
	int doubleSubitems;
	long int numFrames;
	float frameFreq;
	char comments[256];
	long int iFrame;
	int floatSubitems6D;
	int floatSubitems3D;
	float *dataCentroids = NULL;
	char *dataStatus = NULL;
	Position3d *data3D = NULL;
	OptotrakRigid *data6D = NULL;
	int numRigids = 0;
	unsigned int numMarkersConverted;
	unsigned int numRigidsConverted;

	fprintf( stdout, "\nProCMM_Convert\n\n" );

	for( i = 1; i < argc; i++ )
	{
		switch( argv[i][0] )
		{
		case '-':
			{
				switch( argv[i][1] )
				{
				case '3':
					outFile3D = &(argv[i][2]);
					break;

				case 'a':
				case 'i':
				case 'A':
				case 'I':
					nasFile = &(argv[i][2]);
					break;

				case 't':
				case 's':
				case 'T':
				case 'S':
					tipFile = &(argv[i][2]);
					break;

				default:
					fprintf( stderr, "Unknown parameter specified (%s)\n", argv[i] );
				}
			}
			break;

		default:
			// not one of the optional parameters
			// this is one of the mandatory parameters
			// these must be specified in order
			if( !rawFile )
			{
				rawFile = argv[i];
			}
			else if( !rigFile )
			{
				rigFile = argv[i];
			}
			else if( !camFile )
			{
				camFile = argv[i];
			}
			else if( !outFile6D )
			{
				outFile6D = argv[i];
			}
			else
			{
				fprintf( stderr, "Unknown Parameter Spefied (%s)\n", argv[i] );
			}
		}
	}

	if( !rawFile )
	{
		fprintf( stdout, "\tERROR: Raw Data File not specified\n" );
		ShowUsage();
		exit( -2 );
	}

	if( !rigFile )
	{
		fprintf( stdout, "\tERROR: Rigid Body File not specified\n" );
		ShowUsage();
		exit( -2 );
	}

	if( !camFile )
	{
		fprintf( stdout, "\tERROR: Camera File not specified\n" );
		ShowUsage();
		exit( -2 );
	}

	if( !outFile6D )
	{
		fprintf( stdout, "\tERROR: Output 6D Data File not specified\n" );
		ShowUsage();
		exit( -2 );
	}

	fprintf( stdout, "Input Raw Data File  : %s\n", rawFile );
	fprintf( stdout, "Input Rigid Body File: %s\n", rigFile );
	fprintf( stdout, "Input Camera File    : %s\n", camFile );
	if( nasFile )
	{
		fprintf( stdout, "Input AUTOscale File : %s\n", nasFile );
	}
	if( tipFile )
	{
		fprintf( stdout, "Input Tip File       : %s\n", tipFile );
	}
	fprintf( stdout, "Output 6D Data File  : %s\n", outFile6D );
	if( outFile3D )
	{
		fprintf( stdout, "Output 3D Data File  : %s\n", outFile3D );
	}

	fprintf( stdout, "\nProcessing...\n" );

	if( OptotrakSetCameraParameters( markerType, wavelength, modelType ) != OPTO_OK )
	{
		fprintf( stderr, "Error setting Camera Parameters (%d, %d, %d)\n", markerType, wavelength, modelType );
		exit( -3 );
	}

	fprintf( stdout, "...Loading Camera Parameters\n" );
	if( OptotrakLoadCameraParameters( camFile ) != OPTO_OK )
	{
		fprintf( stderr, "Error loading Camera File (%s)\n", camFile );
		exit( -3 );
	}

	if( nasFile )
	{
		fprintf( stdout, "...Loading AUTOscale\n" );
		if( OptotrakLoadAutoScale( nasFile ) != OPTO_OK )
		{
			fprintf( stderr, "Error loading AUTOscale File (%s)\n", nasFile );
			exit( -3 );
		}
	}

	fprintf( stdout, "...Loading Rigid Body File\n" );
	if( RigidBodyAddFromFile( 0, 1, rigFile, 0 ) != OPTO_OK )
	{
		fprintf( stderr, "Error loading Rigid Body File (%s)\n", rigFile );
		exit( -3 );
	}
	numRigids++;

	fprintf( stdout, "...Loading Raw Data File\n" );
	if( OptoFileOpenAll( rawFile, rawFileID, OPEN_READ, &numMarkers, &floatSubitems, &charSubitems, &intSubitems, &doubleSubitems, &numFrames, &frameFreq, comments, NULL ) != OPTO_OK )
	{
		fprintf( stderr, "Error loading Raw Data File (%s)\n", rawFile );
		exit( -3 );
	}

	fprintf( stdout, "...Opening output 6D Data File\n" );
	floatSubitems6D = 7; /* Rx, Ry, Rz, X, Y, Z, err */
	if( OptoFileOpenAll( outFile6D, outFile6DID, OPEN_WRITE, &numRigids, &floatSubitems6D, 0, 0, 0, &numFrames, &frameFreq, comments, NULL ) != OPTO_OK )
	{
		fprintf( stderr, "Error opening output 6D Data File (%s)\n", outFile6D );
		exit( -3 );
	}
	if( outFile3D )
	{
		fprintf( stdout, "...Opening output 3D Data File\n" );
		floatSubitems3D = 3; /* X, Y, Z */
		if( OptoFileOpenAll( outFile3D, outFile3DID, OPEN_WRITE, &numMarkers, &floatSubitems3D, 0, 0, 0, &numFrames, &frameFreq, comments, NULL ) != OPTO_OK )
		{
			fprintf( stderr, "Error opening output 3D Data File (%s)\n", outFile3D );
			exit( -3 );
		}
	}

	fprintf( stdout, "...Initializing OAPI Collection\n" );
	if( OptotrakSetupCollection( numMarkers, frameFreq, OPTO_MAX_MARKERFREQ_CERTUS, 5, 160, 1, 0.15f, 15.0f, 1.0f, 0.0f, 0 ) != OPTO_OK )
	{
		fprintf( stderr, "Error Setting up OAPI collection\n" );
		exit( -3 );
	}

	// allocate memory for one frame of data
	dataCentroids = (float*)malloc( numMarkers * floatSubitems * sizeof(float) );
	dataStatus = (char*)malloc( numMarkers * charSubitems * sizeof(char) );
	data3D = (Position3d*)malloc( numMarkers * sizeof(Position3d) );
	data6D = (OptotrakRigid*)malloc( numRigids * sizeof(OptotrakRigid) );

	for( iFrame = 0; iFrame < numFrames; iFrame++ )
	{
		fprintf( stdout, "......Processing Frame %ld\n", iFrame + 1 );

		if( OptoFileReadAll( rawFileID, iFrame, 1, dataCentroids, dataStatus, NULL, NULL ) != OPTO_OK )
		{
			fprintf( stderr, "Error reading raw data for frame %ld (%s)\n", iFrame + 1, rawFile );
			exit( -4 );
		}

		if( OptotrakConvertCentroidsAndStatusTo6D( &numMarkersConverted, &numRigidsConverted, dataCentroids, dataStatus, data3D, data6D ) != OPTO_OK )
		{
			fprintf( stderr, "Error convert raw data to 6D for frame %ld\n", iFrame + 1 );
			exit( -4 );
		}

		if( OptoFileWriteAll( outFile6DID, iFrame, 1, &(data6D->transformation.euler), NULL, NULL, NULL ) != OPTO_OK )
		{
			fprintf( stderr, "Error writing 6D data for frame %ld (%s)\n", iFrame + 1, outFile6D );
			exit( -4 );
		}

		if( outFile3D )
		{
			if( OptoFileWriteAll( outFile3DID, iFrame, 1, data3D, NULL, NULL, NULL ) != OPTO_OK )
			{
				fprintf( stderr, "Error writing 3D data for frame %ld (%s)\n", iFrame + 1, outFile3D );
				exit( -4 );
			}
		}
	}
	fprintf( stdout, "...Processing Complete\n" );

	if( OptoFileCloseAll( outFile6DID ) != OPTO_OK )
	{
		fprintf( stderr, "Error closing output 6D Data File (%s)\n", outFile6D );
		exit( -3 );
	}
	if( OptoFileCloseAll( rawFileID ) != OPTO_OK )
	{
		fprintf( stderr, "Error closing Raw Data File (%s)\n", rawFile );
		exit( -3 );
	}

} /* main */

