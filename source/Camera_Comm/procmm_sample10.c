/*****************************************************************
Name:             ProCMM_SAMPLE10.C

Description:
 	Optotrak ProCMM OAPI Sample

*****************************************************************/

/*****************************************************************
C Library Files Included
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
void sleep( unsigned int uSec );
#else
#include <unistd.h>
#endif

/*****************************************************************
ND Library Files Included
*****************************************************************/
#include "ndtypes.h"
#include "ndpack.h"
#include "ndopto.h"

/*
 * This are sample frequencies.
 * Depending on the number of markers, the MarkerFrequency may need to be increased.
 * If MarkerFrequency exceeds 4500, the FrameFrequency may need to be decreased
 */
#define SAMPLE_MARKERFREQ		1100.0f
#define SAMPLE_FRAMEFREQ		21.0f
#define SAMPLE_DUTYCYCLE		0.85f
#define SAMPLE_VOLTAGE			15.0f
#define SAMPLE_STREAMDATA		0

#define PROCMM_MARKER_TYPE		7
#define PROCMM_WAVELENGTH_TYPE	1
#define PROCMM_MODEL_TYPE		6


/*****************************************************************
Application Files Included
*****************************************************************/
#include "certus_aux.h"
#include "ot_aux.h"


void main( int argc, char *argv[] )
{
	OptotrakSettings dtSettings;
    char szNDErrorString[MAX_ERROR_STRING_LENGTH + 1];
    int i;
	int nCurDevice;
	int nCurFrame;
	int nDevices;
	int nDeviceMarkers;
	int nRigidBodies;
	int nCurRigid;
	int nMarkersToActivate;
    int nMarkerType;
	int nWavelengthType;
	int nModelType;
	ApplicationDeviceInformation *pdtDevices;
    DeviceHandle *pdtDeviceHandles;
    DeviceHandleInfo *pdtDeviceHandlesInfo;
    unsigned int uFlags;
    unsigned int uElements;
    unsigned int uFrameNumber;
	Position3d *pData3d;
	struct OptotrakRigidStruct *pData6d;
	boolean bTransputerLoad = FALSE;
	int arg;
	unsigned uRigidFlags = OPTOTRAK_RETURN_EULER_FLAG;
	
    /*
     * initialization
     */
	pdtDevices = NULL;
	pdtDeviceHandles = NULL;
	pdtDeviceHandlesInfo = NULL;
	pData3d = NULL;
	pData6d = NULL;
	nMarkersToActivate = 0;
	nDevices = 0;
	nDeviceMarkers = 0;
	nRigidBodies = 0;
	dtSettings.nMarkers = 0;
	dtSettings.fFrameFrequency = SAMPLE_FRAMEFREQ;
	dtSettings.fMarkerFrequency = SAMPLE_MARKERFREQ;
	dtSettings.nThreshold = 5;
	dtSettings.nMinimumGain = 160;
	dtSettings.nStreamData = SAMPLE_STREAMDATA;
	dtSettings.fDutyCycle = SAMPLE_DUTYCYCLE;
	dtSettings.fVoltage = SAMPLE_VOLTAGE;
	dtSettings.fCollectionTime = 1.0;
	dtSettings.fPreTriggerTime = 0.0;

	/*
	 * Announce that the program has started
	 */
	fprintf( stdout, "\nOptotrak PROcmm sample program\n\n" );

	for( arg = 1; arg < argc; arg++ )
	{
		if( strncmp( argv[arg], "-dld", 6 ) == 0 )
		{
			bTransputerLoad = TRUE;
		}
		else if( strncmp( argv[arg], "-q", 2 ) == 0 )
		{
			uRigidFlags = OPTOTRAK_RETURN_QUATERN_FLAG;
		}
		else if( strncmp( argv[arg], "-e", 2 ) == 0 )
		{
			uRigidFlags = OPTOTRAK_RETURN_EULER_FLAG;
		}
		else if( strncmp( argv[arg], "-m", 2 ) == 0 )
		{
			uRigidFlags = OPTOTRAK_RETURN_MATRIX_FLAG;
		}
	}

	fprintf( stdout, "Program Options:\n" );
	fprintf( stdout, "\tTransputer Load: %s\n", bTransputerLoad? "TRUE" : "FALSE" );
	fprintf( stdout, "\tRigid Flags    : 0x%x\n", uRigidFlags );

	if( bTransputerLoad )
	{
		/*
		 * Load the system of processors.
		 */
		fprintf( stdout, "...TransputerLoadSystem\n" );
		if( TransputerLoadSystem( "system" ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */

		sleep( 1 );
	} /* if */

    /*
     * Communication Initialization
     * Once the system processors have been loaded, the application
     * prepares for communication by initializing the system processors.
     */
	fprintf( stdout, "...TransputerInitializeSystem\n" );
    if( TransputerInitializeSystem( OPTO_LOG_ERRORS_FLAG ) != OPTO_NO_ERROR_CODE )
	{
		fprintf( stdout, "...TransputerDetermineSystemCfg\n" );
		if( TransputerDetermineSystemCfg( NULL ) )
		{
			fprintf( stderr, "Error in determining the system parameters.\n" );
			goto ERROR_EXIT;
		} /* if */
		fprintf( stdout, "...TransputerLoadSystem\n" );
		if( TransputerLoadSystem( "system" ) != OPTO_NO_ERROR_CODE )
		{
			fprintf( stderr, "Error in loading system parameters.\n" );
			goto ERROR_EXIT;
		} /* if */
		sleep( 1 );
		if( TransputerInitializeSystem( OPTO_LOG_ERRORS_FLAG ) != OPTO_NO_ERROR_CODE )
		{
			fprintf( stderr, "Error initializing system.\n" );
			goto ERROR_EXIT;
		} /* if */
	} /* if */

	/*
	 * Determine if this sample will run with the system attached.
	 * This sample is intended for Optotrak Certus systems.
	 */
	fprintf( stdout, "...DetermineSystem\n" );
	if( uDetermineSystem( ) != OPTOTRAK_CERTUS_FLAG )
	{
		goto PROGRAM_COMPLETE;
	} /* if */

    /*
     * Strober Initialization
     * Once communication has been initialized, the application must
     * determine the strober configuration.
     * The application retrieves device handles and all strober
     * properties from the system.
     */
	fprintf( stdout, "...DetermineStroberConfiguration\n" );
	if( DetermineStroberConfiguration( &pdtDeviceHandles, &pdtDeviceHandlesInfo, &nDevices ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

    /*
     * Now that all the device handles have been completely set up,
     * the application can store all the device handle information in
     * an internal data structure.  This will facilitate lookups when
     * a property setting needs to be checked.
     */
    ApplicationStoreDeviceProperties( &pdtDevices, pdtDeviceHandlesInfo, nDevices );

	/*
	 * Add rigid body data from device handle
	 */
	for( nCurDevice = 0; nCurDevice < nDevices; nCurDevice++ )
	{
		/*
		 * if the device has a ROM, use the rigid body from the rom to set up the collection
		 */
		if( pdtDevices[nCurDevice].bHasROM )
		{
			fprintf( stdout, "...RigidBodyAddFromDeviceHandle\n" );
			if( RigidBodyAddFromDeviceHandle( pdtDeviceHandlesInfo[nCurDevice].pdtHandle->nID, nRigidBodies, uRigidFlags ) != OPTO_NO_ERROR_CODE )
			{
				goto ERROR_EXIT;
			} /* if */

			nRigidBodies++;
		}
		else
		{
			/* this device has no SROM and should not be activated */
		} /* for */
	} /* for */
	fprintf( stdout, "\n" );

	/*
     * Retrieve the device properties again to verify that the changes took effect.
     */
	for( nCurDevice = 0; nCurDevice < nDevices; nCurDevice++ )
	{
		if( GetDevicePropertiesFromSystem( &(pdtDeviceHandlesInfo[nCurDevice]) ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */
	} /* for */

	if( ApplicationStoreDeviceProperties( &pdtDevices, pdtDeviceHandlesInfo, nDevices ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Retrieve ProCMM Data
	 */
	fprintf( stdout, "\n" );

	/*
	 * check if any devices have been detected by the system
	 */
	if( nDevices == 0 )
	{
		fprintf( stdout, ".........no devices detected.\n" );
		goto PROGRAM_COMPLETE;
	} /* if */

	/*
	 * Determine the collection settings based on the device properties
	 */
	ApplicationDetermineCollectionParameters( nDevices, pdtDevices, &dtSettings );

	/*
	 * Use PROcmm camera parameters
	 * PROcmm cameras use a specific set of marker, wavelength and model
	 */
    nMarkerType     = PROCMM_MARKER_TYPE;
    nWavelengthType = PROCMM_WAVELENGTH_TYPE;
    nModelType      = PROCMM_MODEL_TYPE;
    if( OptotrakSetCameraParameters( nMarkerType, nWavelengthType, nModelType ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
	 * Load camera parameters.
     */
	fprintf( stdout, "...OptotrakLoadCameraParameters\n" );
    if( OptotrakLoadCameraParameters( "standard" ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

    /* If we specify NULL for the autoscale file, this method will attempt
	 * to retrieve the autoscale .nas file programmed into the camera flash.
	 * OPTO_USER_ERROR_CODE may simply indicate that no autoscale file was
	 * programmed to the camera flash. We'll carry on regardless...
	 */
	fprintf( stdout, "...OptotrakLoadAutoScale\n" );
	if( OptotrakLoadAutoScale( NULL ) == OPTO_SYSTEM_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Ensure that we are firing some markers
	 */
	if( dtSettings.nMarkers == 0 )
	{
		fprintf( stdout, "Error: There are no markers to be activated.\n" );
		goto ERROR_EXIT;
	} /* if */

	/*
	 * allocate memory for 3d data
	 */
	pData3d = (Position3d*)malloc( dtSettings.nMarkers * sizeof( Position3d ) );
	pData6d = (struct OptotrakRigidStruct*)malloc( nRigidBodies * sizeof( struct OptotrakRigidStruct ) );

    /*
     * Configure Optotrak Collection
     * Once the system strobers have been enabled, and all settings are
     * loaded, the application can set up the Optotrak collection
     */
	fprintf( stdout, "...OptotrakSetupCollection\n" );
	fprintf( stdout, ".....%d, %.2f, %.0f, %d, %d, %d, %.2f, %.2f, %.0f, %.0f\n",
								 dtSettings.nMarkers,
			                     dtSettings.fFrameFrequency,
				                 dtSettings.fMarkerFrequency,
					             dtSettings.nThreshold,
						         dtSettings.nMinimumGain,
							     dtSettings.nStreamData,
								 dtSettings.fDutyCycle,
								 dtSettings.fVoltage,
								 dtSettings.fCollectionTime,
								 dtSettings.fPreTriggerTime );
    if( OptotrakSetupCollection( dtSettings.nMarkers,
			                     dtSettings.fFrameFrequency,
				                 dtSettings.fMarkerFrequency,
					             dtSettings.nThreshold,
						         dtSettings.nMinimumGain,
							     dtSettings.nStreamData,
								 dtSettings.fDutyCycle,
								 dtSettings.fVoltage,
								 dtSettings.fCollectionTime,
								 dtSettings.fPreTriggerTime,
								 OPTOTRAK_NO_FIRE_MARKERS_FLAG | OPTOTRAK_BUFFER_RAW_FLAG | OPTOTRAK_SWITCH_AND_CONFIG_FLAG | OPTOTRAK_BACKGROUND_SUBTRACT_ON ) != OPTO_NO_ERROR_CODE )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Wait one second to let the camera adjust.
     */
    sleep( 1 );

    /*
     * Prepare for realtime data retrieval.
     * Activate markers. Turn on the markers prior to data retrieval.
     */
	fprintf( stdout, "...OptotrakActivateMarkers\n" );
    if( OptotrakActivateMarkers( ) != OPTO_NO_ERROR_CODE )
    {
        goto ERROR_EXIT;
    } /* if */
	sleep( 1 );

    /*
     * Get and display five frames of 3D data.
     */
    fprintf( stdout, "\n\nSample Program Results:\n\n" );
    for( nCurFrame = 0; nCurFrame < 10; nCurFrame++ )
    {
        /*
         * Get a frame of data.
         */
        fprintf( stdout, "\n" );
		if( DataGetLatestTransforms2( &uFrameNumber, &uElements, &uFlags, pData6d, pData3d ) )
		{
			goto ERROR_EXIT;
		} /* if */

		/*
		 * Check the returned flags member for improper transforms.
		 */
		for( nCurRigid = 0; nCurRigid < nRigidBodies; ++nCurRigid )
		{
			if( pData6d[nCurRigid].flags & OPTOTRAK_UNDETERMINED_FLAG )
			{
				fprintf( stdout, "Undetermined transform!\n" );
				if( pData6d[nCurRigid].flags & OPTOTRAK_RIGID_ERR_MKR_SPREAD )
				{
					fprintf( stdout, " Marker spread error.\n" );
				} /* if */
				break;
			} /* if */
		} /* for */

		/*
		 * Print out the data.
		 */
		fprintf( stdout, "Frame     : %8u\n", uFrameNumber );
		for( nCurRigid = 0; nCurRigid < nRigidBodies; ++nCurRigid )
		{
			fprintf( stdout, "Tool %ld    : ", pData6d[nCurRigid].RigidId + 1 );
			if( pData6d[nCurRigid].flags & OPTOTRAK_UNDETERMINED_FLAG )
			{
				fprintf( stdout, "MISSING.  Undertermined transform.\n\n" );
			}
			else
			{
				if( uRigidFlags & OPTOTRAK_RETURN_EULER_FLAG )
				{
					fprintf( stdout, "X " );
					DisplayFloat( pData6d[nCurRigid].transformation.euler.translation.x );
					fprintf( stdout, "Y " );
					DisplayFloat( pData6d[nCurRigid].transformation.euler.translation.y );
					fprintf( stdout, "Z " );
					DisplayFloat( pData6d[nCurRigid].transformation.euler.translation.z );
					fprintf( stdout, "Rx " );
					DisplayFloat( pData6d[nCurRigid].transformation.euler.rotation.yaw );
					fprintf( stdout, "Ry " );
					DisplayFloat( pData6d[nCurRigid].transformation.euler.rotation.pitch );
					fprintf( stdout, "Rz " );
					DisplayFloat( pData6d[nCurRigid].transformation.euler.rotation.roll );
				}
				else if( uRigidFlags & OPTOTRAK_RETURN_QUATERN_FLAG )
				{
					fprintf( stdout, "X " );
					DisplayFloat( pData6d[nCurRigid].transformation.quaternion.translation.x );
					fprintf( stdout, "Y " );
					DisplayFloat( pData6d[nCurRigid].transformation.quaternion.translation.y );
					fprintf( stdout, "Z " );
					DisplayFloat( pData6d[nCurRigid].transformation.quaternion.translation.z );
					fprintf( stdout, "q0 " );
					DisplayFloat( pData6d[nCurRigid].transformation.quaternion.rotation.q0 );
					fprintf( stdout, "qx " );
					DisplayFloat( pData6d[nCurRigid].transformation.quaternion.rotation.qx );
					fprintf( stdout, "qy " );
					DisplayFloat( pData6d[nCurRigid].transformation.quaternion.rotation.qy );
					fprintf( stdout, "qz " );
					DisplayFloat( pData6d[nCurRigid].transformation.quaternion.rotation.qz );
				}
				else if( uRigidFlags & OPTOTRAK_RETURN_MATRIX_FLAG )
				{
				}
			} /* if */
			fprintf( stdout, "\n" );
		} /* for */
	
    } /* for */
    fprintf( stdout, "\n\n" );

    /*
     * De-activate the markers.
     */
	fprintf( stdout, "...OptotrakDeActivateMarkers\n" );
    if( OptotrakDeActivateMarkers() )
    {
        goto ERROR_EXIT;
    } /* if */

	/*
	 * Stop the collection.
	 */
	fprintf( stdout, "...OptotrakStopCollection\n" );
	if( OptotrakStopCollection( ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */


PROGRAM_COMPLETE:
    /*
     * CLEANUP
     */
	fprintf( stdout, "\n" );
	fprintf( stdout, "...TransputerShutdownSystem\n" );
    TransputerShutdownSystem( );

	/*
	 * free all memory
	 */
	if( pdtDeviceHandlesInfo )
	{
		for( i = 0; i < nDevices; i++ )
		{
			AllocateMemoryDeviceHandleProperties( &(pdtDeviceHandlesInfo[i].grProperties), 0 );
		} /* for */
	} /* if */
	AllocateMemoryDeviceHandles( &pdtDeviceHandles, 0 );
	AllocateMemoryDeviceHandlesInfo( &pdtDeviceHandlesInfo, pdtDeviceHandles, 0 );
	free( pData3d );
	free( pData6d );

	exit( 0 );


ERROR_EXIT:
	/*
	 * Indicate that an error has occurred
	 */
	fprintf( stdout, "\nAn error has occurred during execution of the program.\n" );
    if( OptotrakGetErrorString( szNDErrorString, MAX_ERROR_STRING_LENGTH + 1 ) == 0 )
    {
        fprintf( stdout, szNDErrorString );
    } /* if */

	fprintf( stdout, "\n\n...TransputerShutdownSystem\n" );
	OptotrakDeActivateMarkers( );
	TransputerShutdownSystem( );

	/*
	 * free all memory
	 */
	if( pdtDeviceHandlesInfo )
	{
		for( i = 0; i < nDevices; i++ )
		{
			AllocateMemoryDeviceHandleProperties( &(pdtDeviceHandlesInfo[i].grProperties), 0 );
		} /* for */
	} /* if */
	AllocateMemoryDeviceHandles( &pdtDeviceHandles, 0 );
	AllocateMemoryDeviceHandlesInfo( &pdtDeviceHandlesInfo, pdtDeviceHandles, 0 );
	free( pData3d );
	free( pData6d );

    exit( 1 );

} /* main */

