/*****************************************************************
Name:             ProCMM_CALCRATES.C

Description:
 	Optotrak ProCMM OAPI Sample - Calculate Rates

*****************************************************************/

/*****************************************************************
C Library Files Included
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_FRAMEFREQ	2000

/*****************************************************************
ND Library Files Included
*****************************************************************/
#include "ndtypes.h"
#include "ndopto.h"

void ShowUsage()
{
	fprintf( stdout, "\nProCMM_CalcRates [-m[l|u]Markers] [-ff[l|u]FrameFreq] [-mf[l|u]MarkerFreq] [-bBSMode] [-MAX]\n" );
}

int main( int argc, char *argv[] )
{
	int minMarkers = -1;
	int maxMarkers = -1;
	int minFrameFreq = 1;
	int maxFrameFreq = MAX_FRAMEFREQ;
	int minMarkerFreq = -1;
	int maxMarkerFreq = -1;
	int bsMode = 2; /* OPTOTRAK_BSMODE_ON */
	int i;
	int iMarkers;
	int iFrameFreq;
	int iMarkerFreq;
	float outFrameFreq;
	float outMarkerFreq;
	float outDutyCycle;
	float maxOutFrameFreq;
	float maxOutMarkerFreq;
	float maxOutDutyCycle;
	int maxOutMarkers;
	boolean wirelessMode = TRUE;
	boolean maxRates = FALSE;

	fprintf( stdout, "\nProCMM_CalcRates\n" );

	for( i = 1; i < argc; i++ )
	{
		if( strncmp( argv[i], "-MAX", 4 ) == 0 )
		{
			maxRates = TRUE;
		}
		else if( strncmp( argv[i], "-mfl", 4 ) == 0 )
		{
			minMarkerFreq = atoi( &argv[i][4] );
		}
		else if( strncmp( argv[i], "-mfu", 4 ) == 0 )
		{
			maxMarkerFreq = atoi( &argv[i][4] );
		}
		else if( strncmp( argv[i], "-mf", 3 ) == 0 )
		{
			minMarkerFreq = atoi( &argv[i][3] );
			maxMarkerFreq = atoi( &argv[i][3] );
		}
		else if( strncmp( argv[i], "-ffl", 4 ) == 0 )
		{
			minFrameFreq = atoi( &argv[i][4] );
		}
		else if( strncmp( argv[i], "-ffu", 4 ) == 0 )
		{
			maxFrameFreq = atoi( &argv[i][4] );
		}
		else if( strncmp( argv[i], "-ff", 3 ) == 0 )
		{
			minFrameFreq = atoi( &argv[i][3] );
			maxFrameFreq = atoi( &argv[i][3] );
		}
		else if( strncmp( argv[i], "-ml", 3 ) == 0 )
		{
			minMarkers = atoi( &argv[i][3] );
		}
		else if( strncmp( argv[i], "-mu", 3 ) == 0 )
		{
			maxMarkers = atoi( &argv[i][3] );
		}
		else if( strncmp( argv[i], "-m", 2 ) == 0 )
		{
			minMarkers = atoi( &argv[i][2] );
			maxMarkers = atoi( &argv[i][2] );
		}
		else if( strncmp( argv[i], "-b", 2 ) == 0 )
		{
			bsMode = atoi( &argv[i][2] );
		}
		else if( strncmp( argv[i], "-w", 2 ) == 0 )
		{
			wirelessMode = (atoi( &argv[i][2] ) != 0);
		}
	}

	// if minMarkers is not specified, but maxMarkers is specified,
	// start at numMarkers = 1
	if( ( minMarkers < 0 ) && ( maxMarkers > 0 ) )
	{
		minMarkers = 1;
	}

	// if minMarkers is specified, but maxMarkers is not specified,
	// set maxMarkers to MAX_MARKERS
	if( ( minMarkers > 0 ) && ( maxMarkers < 0 ) )
	{
		maxMarkers = OPTO_MAX_MARKERS;
	}

	// Number of markers is required
	if( ( minMarkers < 0 ) || ( maxMarkers < 0 ) )
	{
		fprintf( stdout, "\n\tNumber of Markers not specified\n" );
		ShowUsage();
		return -1;
	}

	// BSMode can be OFF (0) or ON (2)
	if( bsMode != 0 && bsMode != 2 )
	{
		fprintf( stdout, "\n\tBSMode not supported (%d)\n", bsMode );
		ShowUsage();
		return -1;
	}

	if( ( maxMarkerFreq > 0 ) && ( minMarkerFreq < 0 ) )
	{
		minMarkerFreq = 0;
	}

	if( ( maxMarkerFreq > 0 ) && ( maxFrameFreq > maxMarkerFreq ) )
	{
		maxFrameFreq = maxMarkerFreq;
	}

	fprintf( stdout, "Markers Min          : %d\n", minMarkers );
	fprintf( stdout, "Markers Max          : %d\n", maxMarkers );
	fprintf( stdout, "Frame Frequency Min  : %d\n", minFrameFreq );
	fprintf( stdout, "Frame Frequency Max  : %d\n", maxFrameFreq );
	fprintf( stdout, "Marker Frequency Min : %d\n", minMarkerFreq );
	fprintf( stdout, "Marker Frequency Max : %d\n", maxMarkerFreq );
	fprintf( stdout, "BS Mode              : %s\n", (bsMode == 2)? "ON" : "OFF" );
	fprintf( stdout, "Wireless Enabled     : %s\n", wirelessMode? "TRUE" : "FALSE" );
	fprintf( stdout, "\n" );

	fprintf( stdout, "Markers, FrameFreq, MarkerFreq, DutyCycle\n" );
	for( iMarkers = minMarkers; iMarkers <= maxMarkers; iMarkers++ )
	{
		for( iFrameFreq = minFrameFreq; iFrameFreq <= maxFrameFreq; iFrameFreq++ )
		{
			for( iMarkerFreq = minMarkerFreq; iMarkerFreq <= maxMarkerFreq; iMarkerFreq++ )
			{
				if( OptoCalcRatesProCMM( (float)iFrameFreq, (float)iMarkerFreq, iMarkers, bsMode, wirelessMode, &outFrameFreq, &outMarkerFreq, &outDutyCycle ) == OPTO_OK )
				{
					if( ( ( maxMarkerFreq < 0 ) || ( outMarkerFreq <= maxMarkerFreq ) ) &&
						( ( minMarkerFreq < 0 ) || ( outMarkerFreq >= minMarkerFreq ) ) )
					{
						if( maxRates )
						{
							maxOutFrameFreq = outFrameFreq;
							maxOutMarkerFreq = outFrameFreq;
							maxOutDutyCycle = outDutyCycle;
							maxOutMarkers = iMarkers;
						}
						else
						{
							fprintf( stdout, "%d, %.0f, %.0f, %.2f\n", iMarkers, outFrameFreq, outMarkerFreq, outDutyCycle );
						}
					}
				}
			}
		}
	}

	if( maxRates )
	{
		fprintf( stdout, "%d, %.0f, %.0f, %.2f\n", maxOutMarkers, maxOutFrameFreq, maxOutMarkerFreq, maxOutDutyCycle );
	}

} /* main */

