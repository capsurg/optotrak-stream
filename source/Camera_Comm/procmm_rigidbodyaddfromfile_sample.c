/*****************************************************************
Name:             procmm_rigidbodyadd_sample.C

Description:

    Optotrak Sample Program - Pro CMM RigidBodyAddFromFile (K-Scan).

*****************************************************************/

/*****************************************************************
C Library Files Included
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void sleep( unsigned int uSec );

/*****************************************************************
ND Library Files Included
*****************************************************************/
#include "ndtypes.h"
#include "ndpack.h"
#include "ndopto.h"

/*****************************************************************
Defines:
*****************************************************************/
#define NUM_KSCAN_MARKERS     40
#define NUM_SCANNERS          1

/*****************************************************************
Application Files Included
*****************************************************************/
#include "ot_aux.h"
#include "certus_aux.h"

/*****************************************************************
Static Structures and Types:
*****************************************************************/

/*****************************************************************
Static Routines:
*****************************************************************/

/*****************************************************************
Global Variables:
*****************************************************************/

/*****************************************************************
Name:               main

Input Values:
    int
        argc        :Number of command line parameters.
    unsigned char
        *argv[]     :Pointer array to each parameter.

Output Values:
    None.

Return Value:
    None.

Description:

    Main program routine performs all steps listed in the above
    program description.

*****************************************************************/
void main( int argc, char *argv[] )
{
	int arg = 0;
	int nCurRigid = 0;
	unsigned int i  = 0;
	unsigned int uFlags = 0;
	unsigned int uElements = 0;
	unsigned int uFrameCnt = 0;
	unsigned int uFrameNumber = 0;
	Position3d *pData3d = NULL;
	struct OptotrakRigidStruct *pData6d = NULL;
	char szNDErrorString[MAX_ERROR_STRING_LENGTH] = {'\0'};
	boolean bNoDownload = FALSE;
	unsigned int numFrames = 10;
	int nDevices = 0;
	int nCurDevice = 0;
	ApplicationDeviceInformation *pdtDevices = NULL;
	DeviceHandle *pdtDeviceHandles = NULL;
	DeviceHandleInfo *pdtDeviceHandlesInfo = NULL;

	szNDErrorString[0] = '\0';

	pData3d = NULL;
	pData6d = NULL;

	/*
	 * Announce that the program has started
	 */
	fprintf( stdout, "\nOptotrak Test Program - RigidBodyAddFromFile (K-Scan)\n\n" );

	bNoDownload = FALSE;
	if( argc > 1 )
	{
		/*
		 * iterate through the command line parameters
		 */
		for( arg = 1; arg < argc; arg++ )
		{
			/*
			 * The '-nodld' argument indicates 'no download'
			 */
			if( strncmp( argv[arg], "-nodld", 6 ) == 0 )
			{
				bNoDownload = TRUE;
			}

			if( strncmp( argv[arg], "-numFrames", 10 ) == 0 )
			{
				arg++;

				/*
				 * Ensure that the user did not forget the parameter
				 */
				if( arg == argc )
				{
					fprintf( stdout, "ERROR: The -numFrames parameter requires a marker number" );
					goto ERROR_EXIT;
				}

				numFrames = atoi( argv[arg] );

				if( numFrames <= 0 )
				{
					fprintf( stdout, "ERROR: The -numFrames value is out of bounds (1+)" );
					goto ERROR_EXIT;
				}
			}
		}
	}

	/*
	 * the -nodld parameter indicates 'no download'
	 */
	if( !bNoDownload )
	{
		/*
		 * Load the system of processors.
		 */
		fprintf( stdout, "...TransputerLoadSystem\n" );
		if( TransputerLoadSystem( "system" ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */

		sleep( 1 );
	} /* if */

    /*
     * Wait one second to let the system finish loading.
     */
    sleep( 1 );

    /*
     * Initialize the processors system.
     */
	fprintf( stdout, "...TransputerInitializeSystem\n" );
    if( TransputerInitializeSystem( OPTO_LOG_ERRORS_FLAG | OPTO_LOG_STATUS_FLAG ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Set optional processing flags (this overides the settings in Optotrak.INI).
     */
	fprintf( stdout, "...OptotrakSetProcessingFlags\n" );
    if( OptotrakSetProcessingFlags( OPTO_LIB_POLL_REAL_DATA |
                                    OPTO_CONVERT_ON_HOST |
                                    OPTO_RIGID_ON_HOST ) )
    {
        goto ERROR_EXIT;
    } /* if */

	/*
	 * Set the camera parameters
	 */
	fprintf( stdout, "...OptotrakSetCameraParameters\n" );
    if( OptotrakSetCameraParameters( 2, 1, 6 ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Load the standard camera parameters.
     */
	fprintf( stdout, "...OptotrakLoadCameraParameters\n" );
    if( OptotrakLoadCameraParameters( "standard" ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Strober Initialization
     * Once communication has been initialized, the application must
     * determine the strober configuration.
     * The application retrieves device handles and all strober
     * properties from the system.
     */
	fprintf( stdout, "...DetermineStroberConfiguration\n" );
	if( DetermineStroberConfiguration( &pdtDeviceHandles, &pdtDeviceHandlesInfo, &nDevices ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

    /*
     * Now that all the device handles have been completely set up,
     * the application can store all the device handle information in
     * an internal data structure.  This will facilitate lookups when
     * a property setting needs to be checked.
     */
    ApplicationStoreDeviceProperties( &pdtDevices, pdtDeviceHandlesInfo, nDevices );

	/*
	 * Change the number of markers to fire for all devices
	 */
	for( nCurDevice = 0; nCurDevice < nDevices; nCurDevice++ )
	{
		if( pdtDevices[nCurDevice].nDeviceType == DH_DEVICE_TYPE_TOOL )
		{
			fprintf( stdout, "...adding %d markers to device %s", NUM_KSCAN_MARKERS, pdtDevices[nCurDevice].szName );
			if( SetMarkersToActivateForDevice( &(pdtDevices[nCurDevice]), pdtDeviceHandlesInfo[nCurDevice].pdtHandle->nID, NUM_KSCAN_MARKERS ) != 0 )
			{
				goto ERROR_EXIT;
			}
		}
	} /* if */
	fprintf( stdout, "\n" );


    /*
     * Set up a collection for the Optotrak.
     */
	fprintf( stdout, "...OptotrakSetupCollection\n" );
    if( OptotrakSetupCollection(
            NUM_KSCAN_MARKERS,   /* Number of markers in the collection. */
            (float)40.0,         /* Frequency to collect data frames at. */
            (float)3900.0,       /* Marker frequency for marker maximum on-time. */
            5,                   /* Dynamic or Static Threshold value to use. */
            160,                 /* Minimum gain code amplification to use. */
            1,                   /* Stream mode for the data buffers. */
            (float)0.65,         /* Marker Duty Cycle to use. */
            (float)15.0,         /* Voltage to use when turning on markers. */
            (float)1.0,          /* Number of seconds of data to collect. */
            (float)0.0,          /* Number of seconds to pre-trigger data by. */
            OPTOTRAK_NO_FIRE_MARKERS_FLAG | OPTOTRAK_BUFFER_RAW_FLAG ) )
    {
        goto ERROR_EXIT;
    } /* if */

	pData3d = (Position3d*)malloc( NUM_KSCAN_MARKERS * sizeof( Position3d ) );
	pData6d = (struct OptotrakRigidStruct*)malloc( NUM_SCANNERS * sizeof( struct OptotrakRigidStruct ) );


    /*
     * Wait one second to let the camera adjust.
     */
    sleep( 1 );

    /*
     * Activate the markers.
     */
	fprintf( stdout, "...OptotrakActivateMarkers\n" );
    if( OptotrakActivateMarkers() )
    {
        goto ERROR_EXIT;
    } /* if */
	sleep( 1 );

    /*
     * Add rigid body for tracking to the Optotrak system from a .RIG file.
     */
	fprintf( stdout, "...RigidBodyAddFromFile\n" );
    if( RigidBodyAddFromFile( 0,
							  1,                  /* First marker in the rigid body.*/
							  "kscan.rom",        /* RIG file containing rigid body coordinates.*/
							  OPTOTRAK_QUATERN_RIGID_FLAG ) )
    {
        goto ERROR_EXIT;
    } /* if */

    fprintf( stdout, "\n\nSample Program Results:\n\n" );

    /*
     * Get and display frames of rigid body data.
     */
    for( uFrameCnt = 0; uFrameCnt < numFrames; ++uFrameCnt )
    {
        /*
         * Get a frame of data.
         */
        if( DataGetLatestTransforms2( &uFrameNumber, &uElements, &uFlags, pData6d, pData3d ) )
        {
            goto ERROR_EXIT;
        } /* if */

		/*
		 * Check the returned flags member for improper transforms.
		 */
		for( nCurRigid = 0; nCurRigid < NUM_SCANNERS; ++nCurRigid )
		{
			if( pData6d[nCurRigid].flags & OPTOTRAK_UNDETERMINED_FLAG )
			{
				fprintf( stdout, "Undetermined transform!\n" );
				if( pData6d[nCurRigid].flags & OPTOTRAK_RIGID_ERR_MKR_SPREAD )
				{
					fprintf( stdout, " Marker spread error.\n" );
				} /* if */
				break;
			} /* if */
		} /* for */

		/*
		 * Print out the valid data.
		 */
		fprintf( stdout, "\n" );
		fprintf( stdout, "Frame     : %8u\n", uFrameNumber );

		for( i = 0; i < NUM_KSCAN_MARKERS; i++ )
		{
			DisplayMarker( i, pData3d[i] );
		} /* for */
		fprintf( stdout, "\n" );

		for( nCurRigid = 0; nCurRigid < NUM_SCANNERS; ++nCurRigid )
		{
			fprintf( stdout, "K-Scan %ld  : ", pData6d[nCurRigid].RigidId + 1 );
			if( pData6d[nCurRigid].flags & OPTOTRAK_UNDETERMINED_FLAG )
			{
				fprintf( stdout, "MISSING.  Undertermined transform.\n\n" );
			}
			else
			{
				fprintf( stdout, "X " );
				DisplayFloat( pData6d[nCurRigid].transformation.euler.translation.x );
				fprintf( stdout, "Y " );
				DisplayFloat( pData6d[nCurRigid].transformation.euler.translation.y );
				fprintf( stdout, "Z " );
				DisplayFloat( pData6d[nCurRigid].transformation.euler.translation.z );
				fprintf( stdout, "Rx " );
				DisplayFloat( pData6d[nCurRigid].transformation.euler.rotation.yaw );
				fprintf( stdout, "Ry " );
				DisplayFloat( pData6d[nCurRigid].transformation.euler.rotation.pitch );
				fprintf( stdout, "Rz " );
				DisplayFloat( pData6d[nCurRigid].transformation.euler.rotation.roll );
			} /* if */
			fprintf( stdout, "\n" );
		} /* for */

		fprintf( stdout, "\n" );
    } /* for */
    fprintf( stdout, "\n" );

    /*
     * De-activate the markers.
     */
	fprintf( stdout, "...OptotrakDeActivateMarkers\n" );
    if( OptotrakDeActivateMarkers() )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Shutdown the processors message passing system.
     */
	fprintf( stdout, "...TransputerShutdownSystem\n" );
    if( TransputerShutdownSystem() )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Exit the program.
     */
    fprintf( stdout, "\nProgram execution complete.\n" );

	if( pData3d ) free( pData3d );
	if( pData6d) free( pData6d );
    exit( 0 );

ERROR_EXIT:
	/*
	 * Indicate that an error has occurred
	 */
	fprintf( stdout, "\nAn error has occurred during execution of the program.\n" );
    if( OptotrakGetErrorString( szNDErrorString, MAX_ERROR_STRING_LENGTH ) == 0 )
    {
        fprintf( stdout, szNDErrorString );
    } /* if */

	fprintf( stdout, "\n\n...TransputerShutdownSystem\n" );
    OptotrakDeActivateMarkers();
    TransputerShutdownSystem();

	if( pData3d ) free( pData3d );
	if( pData6d) free( pData6d );

    exit( 1 );

} /* main */
