/*****************************************************************
Name:             procmm_rigidbodyadd_sample.C

Description:

    Optotrak Sample Program - Pro CMM RigidBodyAdd.

*****************************************************************/

/*****************************************************************
C Library Files Included
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
void sleep( unsigned int uSec );
#else
#include <unistd.h>
#endif

/*****************************************************************
ND Library Files Included
*****************************************************************/
#include "ndtypes.h"
#include "ndpack.h"
#include "ndopto.h"

/*****************************************************************
Defines:
*****************************************************************/

#define NUM_MARKERS     15
#define FRAME_RATE      (float)21.0
#define COLLECTION_TIME (float)5.0

/*
 * Constants for identifying the rigid bodies.
 */
#define NUM_RIGID_BODIES    2
#define RIGID_BODY_1        0
#define RIGID_BODY_2        1

/*****************************************************************
Application Files Included
*****************************************************************/
#include "ot_aux.h"

/*****************************************************************
Static Structures and Types:
*****************************************************************/


/*
 * Type definition to retreive and access rigid body transformation
 * data.
 */
typedef struct RigidBodyDataStruct
{
    struct OptotrakRigidStruct  pRigidData[NUM_RIGID_BODIES];
    Position3d                  p3dData[NUM_MARKERS];
} RigidBodyDataType;

/*****************************************************************
Static Routines:
*****************************************************************/

/*****************************************************************
Global Variables:
*****************************************************************/
#ifdef __BORLANDC__
extern unsigned _stklen = 16000;
#endif

/*
 * Modify these values with the attached rigid body's definition
 */
static Position3d
    RigidBodyDef[NUM_MARKERS] =
    {
        { -0.000200f, -54.533600f, 233.679199f },
        {  0.073000f, -30.017599f,  49.830299f },
        { -0.014900f, -10.939100f,  11.035500f },
        {  0.024000f,  29.859501f,  28.481199f },
        {  0.027200f,  51.233700f, 212.498306f },
        { -0.070200f, -65.580597f, 222.603500f },
        { -0.039600f, -41.076900f,  38.980499f },
        { -0.044500f,   0.064700f,  -0.164100f },
        { -0.083200f,  40.774300f,  39.271301f },
        { -0.079100f,  62.479099f, 223.396606f },
        {  0.006500f, -54.675598f, 211.535400f },
        { -0.036200f, -30.169201f,  27.832300f },
        { -0.072100f,  11.240700f,  11.107500f },
        {  0.096900f,  29.916000f,  50.161499f },
        { -0.002400f,  51.360401f, 234.581100f },
    };

/*
0                 0.000000     -0.015300     15.482100    1
1                 0.000000      0.056900      0.023700    1
2                 0.000000     15.603500     -0.015100    1
*/

/*****************************************************************
Name:               main

Input Values:
    int
        argc        :Number of command line parameters.
    unsigned char
        *argv[]     :Pointer array to each parameter.

Output Values:
    None.

Return Value:
    None.

Description:

    Main program routine performs all steps listed in the above
    program description.

*****************************************************************/
void main( int argc, char *argv[] )
{
	int arg;
    unsigned int
		i,
        uFlags,
        uElements,
        uFrameCnt,
        uRigidCnt,
        uMarkerCnt,
        uFrameNumber;
	Position3d *p3dData = NULL;
    RigidBodyDataType RigidBodyData;
    char szNDErrorString[MAX_ERROR_STRING_LENGTH + 1];
	boolean bNoDownload;
	int nMissingMarker;
	unsigned int numFrames = 10;

	szNDErrorString[0] = '\0';

	/*
	 * Announce that the program has started
	 */
	fprintf( stdout, "\nOptotrak Test Program - RigidBodyAdd\n\n" );

	bNoDownload = FALSE;
	if( argc > 1 )
	{
		/*
		 * iterate through the command line parameters
		 */
		for( arg = 1; arg < argc; arg++ )
		{
			/*
			 * The '-nodld' argument indicates 'no download'
			 */
			if( strncmp( argv[arg], "-nodld", 6 ) == 0 )
			{
				bNoDownload = TRUE;
			}

			if( strncmp( argv[arg], "-numFrames", 10 ) == 0 )
			{
				arg++;

				/*
				 * Ensure that the user did not forget the parameter
				 */
				if( arg == argc )
				{
					fprintf( stdout, "ERROR: The -numFrames parameter requires a marker number" );
					goto ERROR_EXIT;
				}

				numFrames = atoi( argv[arg] );

				if( numFrames <= 0 )
				{
					fprintf( stdout, "ERROR: The -numFrames value is out of bounds (1+)" );
					goto ERROR_EXIT;
				}
			}

			/*
			 * The '-setmissing' argument indicats that a marker is set to MISSING in the input rigid body
			 */
			if( strncmp( argv[arg], "-setmissing", 11 ) == 0 )
			{
				arg++;

				/*
				 * Ensure that the user did not forget the parameter
				 */
				if( arg == argc )
				{
					fprintf( stdout, "ERROR: The -setmissing parameter requires a marker number" );
					goto ERROR_EXIT;
				}

				nMissingMarker = atoi( argv[arg] );

				/*
				 * Ensure that the marker index is valid
				 */
				if( nMissingMarker < 1 || nMissingMarker > NUM_MARKERS )
				{
					fprintf( stdout, "ERROR: The -setmissing marker index is out of bounds (1-%d)", NUM_MARKERS );
					goto ERROR_EXIT;
				}

				RigidBodyDef[nMissingMarker - 1].x = BAD_FLOAT;
				RigidBodyDef[nMissingMarker - 1].y = BAD_FLOAT;
				RigidBodyDef[nMissingMarker - 1].z = BAD_FLOAT;
			}
		}
	}

	fprintf( stdout, "\nRigid Body To Add:\n" );
	for( i = 0; i < NUM_MARKERS; i++ )
	{
		if( isMissing(RigidBodyDef[i].x))
		{
			fprintf( stdout, "M_%02d: MISSING\n", i + 1 );
		}
		else
		{
			fprintf( stdout, "M_%02d: X = %9.3f   Y = %9.3f   Z = %9.3f\n", i + 1, RigidBodyDef[i].x, RigidBodyDef[i].y, RigidBodyDef[i].z );
		}
	}
	fprintf( stdout, "\n\n" );

	/*
	 * the -nodld parameter indicates 'no download'
	 */
	if( !bNoDownload )
	{
		/*
		 * Load the system of processors.
		 */
		fprintf( stdout, "...TransputerLoadSystem\n" );
		if( TransputerLoadSystem( "system" ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */

		sleep( 1 );
	} /* if */

    /*
     * Wait one second to let the system finish loading.
     */
    sleep( 1 );

    /*
     * Initialize the processors system.
     */
	fprintf( stdout, "...TransputerInitializeSystem\n" );
    if( TransputerInitializeSystem( OPTO_LOG_ERRORS_FLAG | OPTO_LOG_STATUS_FLAG ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Set optional processing flags (this overides the settings in Optotrak.INI).
     */
	fprintf( stdout, "...OptotrakSetProcessingFlags\n" );
    if( OptotrakSetProcessingFlags( OPTO_LIB_POLL_REAL_DATA |
                                    OPTO_CONVERT_ON_HOST |
                                    OPTO_RIGID_ON_HOST ) )
    {
        goto ERROR_EXIT;
    } /* if */

	/*
	 * Set the camera parameters
	 */
	fprintf( stdout, "...OptotrakSetCameraParameters\n" );
    if( OptotrakSetCameraParameters( 2, 1, 6 ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Load the standard camera parameters.
     */
	fprintf( stdout, "...OptotrakLoadCameraParameters\n" );
    if( OptotrakLoadCameraParameters( "standard" ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Set up a collection for the Optotrak.
     */
	fprintf( stdout, "...OptotrakSetupCollection\n" );
    if( OptotrakSetupCollection(
            NUM_MARKERS,        /* Number of markers in the collection. */
            FRAME_RATE,         /* Frequency to collect data frames at. */
            (float)2500.0,      /* Marker frequency for marker maximum on-time. */
            30,                 /* Dynamic or Static Threshold value to use. */
            160,                /* Minimum gain code amplification to use. */
            1,                  /* Stream mode for the data buffers. */
            (float)0.4,         /* Marker Duty Cycle to use. */
            (float)7.5,         /* Voltage to use when turning on markers. */
            COLLECTION_TIME,    /* Number of seconds of data to collect. */
            (float)0.0,         /* Number of seconds to pre-trigger data by. */
            OPTOTRAK_NO_FIRE_MARKERS_FLAG | OPTOTRAK_BUFFER_RAW_FLAG ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Wait one second to let the camera adjust.
     */
    sleep( 1 );

    /*
     * Activate the markers.
     */
	fprintf( stdout, "...OptotrakActivateMarkers\n" );
    if( OptotrakActivateMarkers() )
    {
        goto ERROR_EXIT;
    } /* if */
	sleep( 1 );

    /*
     * Add rigid body for tracking to the Optotrak system from an array of
     * 3D points.
     */
	fprintf( stdout, "...RigidBodyAdd\n" );
    if( RigidBodyAdd(
            RIGID_BODY_1,           /* ID associated with this rigid body. */
            1,                      /* First marker in the rigid body. */
            15,                      /* Number of markers in the rigid body. */
            (float *)RigidBodyDef,    /* 3D coords for each marker in the body. */
            NULL,                   /* no normals for this rigid body. */
            OPTOTRAK_QUATERN_RIGID_FLAG ) )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Add rigid body for tracking to the Optotrak system from a .RIG file.
     */
	fprintf( stdout, "...RigidBodyAddFromFile\n" );
    if( RigidBodyAddFromFile(
            RIGID_BODY_2,   /* ID associated with this rigid body.*/
            1,              /* First marker in the rigid body.*/
            "probe_nocoordinates.rig",        /* RIG file containing rigid body coordinates.*/
            OPTOTRAK_QUATERN_RIGID_FLAG ) )
    {
        goto ERROR_EXIT;
    } /* if */

	/*
	 * Allocate data for 3D positions
	 */
	p3dData = (Position3d*)malloc( NUM_MARKERS * sizeof( Position3d ) );

    fprintf( stdout, "\n\nSample Program Results:\n\n" );

	/*
	 * Use DataGetLatest3D to obtain frames of data
	 */
    for( uFrameCnt = 0; uFrameCnt < numFrames; ++uFrameCnt )
    {
		if( DataGetLatest3D( &uFrameNumber, &uElements,&uFlags, p3dData ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		}

        for( uMarkerCnt = 0; uMarkerCnt < NUM_MARKERS; ++uMarkerCnt )
        {
			DisplayMarker( uMarkerCnt + 1, p3dData[uMarkerCnt] );
        } /* for */

		fprintf( stdout, "\n" );
	}

    /*
     * Get and display frames of rigid body data.
     */
    fprintf( stdout, "Rigid Body Data Display\n" );
    for( uFrameCnt = 0; uFrameCnt < numFrames; ++uFrameCnt )
    {
        /*
         * Get a frame of data.
         */
        if( DataGetLatestTransforms( &uFrameNumber, &uElements, &uFlags, &RigidBodyData ) )
        {
            goto ERROR_EXIT;
        } /* if */

        /*
         * Print out the rigid body transformation data.
         */
        fprintf( stdout, "\n" );
        for( uRigidCnt = 0; uRigidCnt < uElements; ++uRigidCnt )
        {
            fprintf( stdout, "Rigid Body %ld : ",
                     RigidBodyData.pRigidData[ uRigidCnt].RigidId );
			fprintf( stdout, " 0x%04ld (%lu) ", RigidBodyData.pRigidData[uRigidCnt].flags, RigidBodyData.pRigidData[uRigidCnt].flags );
			if( RigidBodyData.pRigidData[uRigidCnt].flags & OPTOTRAK_UNDETERMINED_FLAG )
			{
				fprintf( stdout, "MISSING (UNDETERMINED_FLAG)\n" );
			}
			else
			{
				fprintf( stdout, "XT = %8.2f YT = %8.2f ZT = %8.2f\n",
							RigidBodyData.pRigidData[ uRigidCnt].transformation.
								euler.translation.x,
							RigidBodyData.pRigidData[ uRigidCnt].transformation.
								euler.translation.y,
							RigidBodyData.pRigidData[ uRigidCnt].transformation.
								euler.translation.z );
			}
        } /* for */

        /*
         * Print out the 3D data.
         */
        fprintf( stdout, "\nAssociated 3D Marker Data:\n" );
        for( uMarkerCnt = 0; uMarkerCnt < NUM_MARKERS; ++uMarkerCnt )
        {
			DisplayMarker( uMarkerCnt + 1, RigidBodyData.p3dData[uMarkerCnt] );
        } /* for */
    } /* for */
    fprintf( stdout, "\n" );

    /*
     * De-activate the markers.
     */
	fprintf( stdout, "...OptotrakDeActivateMarkers\n" );
    if( OptotrakDeActivateMarkers() )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Shutdown the processors message passing system.
     */
	fprintf( stdout, "...TransputerShutdownSystem\n" );
    if( TransputerShutdownSystem() )
    {
        goto ERROR_EXIT;
    } /* if */

    /*
     * Exit the program.
     */
    fprintf( stdout, "\nProgram execution complete.\n" );

	if( p3dData ) free( p3dData );

    exit( 0 );

ERROR_EXIT:
	/*
	 * Indicate that an error has occurred
	 */
	fprintf( stdout, "\nAn error has occurred during execution of the program.\n" );
    if( OptotrakGetErrorString( szNDErrorString,
                                MAX_ERROR_STRING_LENGTH + 1 ) == 0 )
    {
        fprintf( stdout, szNDErrorString );
    } /* if */

	fprintf( stdout, "\n\n...TransputerShutdownSystem\n" );
    OptotrakDeActivateMarkers();
    TransputerShutdownSystem();

	if( p3dData ) free( p3dData );

    exit( 1 );

} /* main */
