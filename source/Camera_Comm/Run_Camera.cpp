#include "Run_Camera.h"

//This function shuts down the system and exits the program when it encounters an error
void Opto::Error_Exit()
{
	char szNDErrorString[MAX_ERROR_STRING_LENGTH + 1];

	std::cout << "An error has occurred during execution of the program." << std::endl;
	if (OptotrakGetErrorString(szNDErrorString,
		MAX_ERROR_STRING_LENGTH + 1) == 0)
	{
		std::cout << szNDErrorString << std::endl;
	} /* if */

	std::cout << "Transputer Shutting Down" << std::endl;
	OptotrakDeActivateMarkers();
	TransputerShutdownSystem();

	system("pause");
	exit(1);
}

//This function retrieves the timestamp to be sent with the data 
std::string Opto::getTimestamp()
{
	std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds> (std::chrono::system_clock::now().time_since_epoch());
	std::string time = std::to_string(ms.count());
	return time;
}

void Opto::initialize(int argc, const char *argv[], const int NumMarkers, float frequency)
{
	/* look for the -nodld parameter that indicates 'no download' */
	if ((argc < 2) || (strncmp(argv[1], "-nodld", 6) != 0))
	{
		/* Load the system of processors */
		std::cout << "Transputer Loading" << std::endl;
		if (TransputerLoadSystem("system") != OPTO_NO_ERROR_CODE)
		{
			Error_Exit();
		}

		Sleep(1000);
	}

	Sleep(1000);

	/* Initialize the processors system */
	std::cout << "Transputer Initializing" << std::endl;
	if (TransputerInitializeSystem(OPTO_LOG_ERRORS_FLAG))

	{
		Error_Exit();
	}

	/* Set optional processing flags (this overides the settings in Optotrak.INI) */
	std::cout << "Setting Optotrak Processing Flags" << std::endl;
	if (OptotrakSetProcessingFlags(OPTO_LIB_POLL_REAL_DATA |
		OPTO_CONVERT_ON_HOST |
		OPTO_RIGID_ON_HOST))
	{
		Error_Exit();
	}

	/* Load the standard camera parameters */
	std::cout << "Loading Optotrak Camera Parameters" << std::endl;
	if (OptotrakLoadCameraParameters("standard"))
	{
		Error_Exit();
	}

	/* Set up a collection for the Optotrak */
	std::cout << "Setting Up Optotrak Collection" << std::endl;
	if (OptotrakSetupCollection(
		NumMarkers,		    /* Number of markers in the collection. */
		frequency,			/* Frequency to collect data frames at. */
		(float)2500.0,      /* Marker frequency for marker maximum on-time. */
		30,                 /* Dynamic or Static Threshold value to use. */
		160,                /* Minimum gain code amplification to use. */
		0,                  /* Stream mode for the data buffers. */
		(float)0.4,         /* Marker Duty Cycle to use. */
		(float)7.5,         /* Voltage to use when turning on markers. */
		(float)1.0,			/* Number of seconds of data to collect. */
		(float)0.0,         /* Number of seconds to pre-trigger data by. */
		OPTOTRAK_NO_FIRE_MARKERS_FLAG | OPTOTRAK_BUFFER_RAW_FLAG))
	{
		Error_Exit();
	}
	Sleep(1000);
}

//This function initializes the rigid body and defines its settings for calculation, this is where you tell it how many markers there 
//are in your rigid body,wht marker number is starts on, point it toward the correct .rig file, etc. In this function, we also define
//what type of daata we want from the rigid body (euler, quaternion, or rot matrix)
void Opto::initRigidBody(int rigidBodyNum)
{
	std::cout << "Adding Rigid Body" << std::endl;
	if (RigidBodyAddFromFile(
		rigidBodyNum,
		3*rigidBodyNum + 1,
		"smart_01",
		0))
	{
		Error_Exit();
	}

	std::cout << "Changing Rigid Body Setting" << std::endl;
	if (RigidBodyChangeSettings(
		rigidBodyNum,	/* ID associated with this rigid body. */
		3,              /* Minimum number of markers which must be seen before performing rigid body calculations.*/
		60,             /* Cut off angle for marker inclusion in calcs.*/
		(float)1.0,     /* Maximum 3-D marker error for this rigid body. */
		(float)0.0,     /* Maximum raw sensor error for this rigid body. */
		(float)1.0,     /* Maximum 3-D RMS marker error for this rigid body. */
		(float)0.0,     /* Maximum raw sensor RMS error for this rigid body. */
		//OPTOTRAK_QUATERN_RIGID_FLAG | OPTOTRAK_RETURN_QUATERN_FLAG))
		OPTOTRAK_DO_RIGID_CALCS_FLAG | OPTOTRAK_RETURN_MATRIX_FLAG))

	{
		Error_Exit();
	}

	Sleep(1000);
}

//This function activates the LEDs in the markers, should be called just before requesting data
void Opto::ActivateMarkers()
{
	std::cout << "Activating Markers" << std::endl;
	if (OptotrakActivateMarkers())
	{
		Error_Exit();
	}
}


//This function shuts down the system and exits the program safely when the program completes
void Opto::NormExit()
{
	/* De-activate the markers */
	std::cout << "Deactivating Markers" << std::endl;
	if (OptotrakDeActivateMarkers())
	{
		Error_Exit();
	}

	/* Shutdown the processors message passing system*/
	std::cout << "Shutting Down Transputer" << std::endl;
	if (TransputerShutdownSystem())
	{
		Error_Exit();
	}

	/* Exit the program*/
	std::cout << "Program execution complete" << std::endl;
	system("pause");
	exit(0);
}

//This function retrieves xyz position data for each marker and returns a message to be sent (or printed) of form marker #, x, y, z;
std::string Opto::MyDisplayMarker(int nMarker, Position3d dtPosition3d)
{
	std::string message;
	for (int i(0); i < nMarker; i++)
	{
		message = std::to_string(nMarker) + "," + std::to_string(dtPosition3d.x) + "," + std::to_string(dtPosition3d.y) + "," + std::to_string(dtPosition3d.z);
	}

	return message;
}
