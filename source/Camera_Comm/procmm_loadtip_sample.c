/*****************************************************************
Name:             ProCMM_loadtip_SAMPLE.C

Description:
 	Optotrak ProCMM OAPI Sample

	Examples for loading TIP files using the OAPI

*****************************************************************/

/*****************************************************************
C Library Files Included
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
void sleep( unsigned int uSec );
#else
#include <unistd.h>
#endif

/*****************************************************************
ND Library Files Included
*****************************************************************/
#include "ndtypes.h"
#include "ndpack.h"
#include "ndopto.h"

/*****************************************************************
Application Files Included
*****************************************************************/
#include "certus_aux.h"
#include "ot_aux.h"

#define MAX_TIP_ID	3

void main( int argc, char *argv[] )
{	
    char szNDErrorString[MAX_ERROR_STRING_LENGTH + 1];
	int i;
	int nDevices;
	int nCurDevice;
	int totalRigids;
	int tipID;
	boolean bSROM;
	ApplicationDeviceInformation *pdtDevices;
    DeviceHandle *pdtDeviceHandles;
    DeviceHandleInfo *pdtDeviceHandlesInfo;
	DeviceHandleProperty dtProperty;
	Position3d tip;
	float tipDiameter;

    /*
     * initialization
     */
	pdtDevices = NULL;
	pdtDeviceHandles = NULL;
	pdtDeviceHandlesInfo = NULL;
	nDevices = 0;
	totalRigids = 0;

	/*
	 * Announce that the program has started
	 */
	fprintf( stdout, "\nOptotrak PROcmm load tip sample program\n\n" );

	/*
	 * look for the -nodld parameter that indicates 'no download'
	 */
	if( ( argc < 2 ) || ( strncmp( argv[1], "-nodld", 6 ) != 0 ) )
	{
		/*
		 * Load the system of processors.
		 */
		fprintf( stdout, "...TransputerLoadSystem\n" );
		if( TransputerLoadSystem( "system" ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */

		sleep( 1 );
	} /* if */

    /*
     * Communication Initialization
     * Once the system processors have been loaded, the application
     * prepares for communication by initializing the system processors.
     */
	fprintf( stdout, "...TransputerInitializeSystem\n" );
    if( TransputerInitializeSystem( OPTO_LOG_ERRORS_FLAG | OPTO_LOG_STATUS_FLAG ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Determine if this sample will run with the system attached.
	 * This sample is intended for Optotrak Certus systems.
	 */
	fprintf( stdout, "...DetermineSystem\n" );
	if( uDetermineSystem( ) != OPTOTRAK_CERTUS_FLAG )
	{
		goto PROGRAM_COMPLETE;
	} /* if */

    /*
     * Strober Initialization
     * Once communication has been initialized, the application must
     * determine the strober configuration.
     * The application retrieves device handles and all strober
     * properties from the system.
     */
	fprintf( stdout, "...DetermineStroberConfiguration\n" );
	if( DetermineStroberConfiguration( &pdtDeviceHandles, &pdtDeviceHandlesInfo, &nDevices ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

    /*
     * Now that all the device handles have been completely set up,
     * the application can store all the device handle information in
     * an internal data structure.  This will facilitate lookups when
     * a property setting needs to be checked.
     */
    ApplicationStoreDeviceProperties( &pdtDevices, pdtDeviceHandlesInfo, nDevices );

	/*
     * Retrieve the device properties again to verify that the changes took effect.
     */
	for( nCurDevice = 0; nCurDevice < nDevices; nCurDevice++ )
	{
		if( GetDevicePropertiesFromSystem( &(pdtDeviceHandlesInfo[nCurDevice]) ) != OPTO_NO_ERROR_CODE )
		{
			goto ERROR_EXIT;
		} /* if */
	} /* for */

	if( ApplicationStoreDeviceProperties( &pdtDevices, pdtDeviceHandlesInfo, nDevices ) != OPTO_NO_ERROR_CODE )
	{
		goto ERROR_EXIT;
	} /* if */

	/*
	 * Retrieve ProCMM Data
	 */
	fprintf( stdout, "\n" );

	/*
	 * check if any devices have been detected by the system
	 */
	if( nDevices == 0 )
	{
		fprintf( stdout, ".........no devices detected.\n" );
		goto PROGRAM_COMPLETE;
	} /* if */

	fprintf( stdout, "DEVICES:\n" );
	for( i = 0; i < nDevices; i++ )
	{
		if( OptotrakDeviceHandleGetProperty( pdtDeviceHandles[i].nID, &dtProperty, DH_PROPERTY_HAS_ROM ) )
		{
			goto ERROR_EXIT;
		} /* if */
		bSROM = ( dtProperty.dtData.nData != 0 );
		if( bSROM )
		{
			// Load the TIP file from Device Handle
			if( OptotrakDeviceHandleLoadTip( pdtDeviceHandles[i].nID, &tip, &tipDiameter ) != OPTO_OK )
			{
				fprintf( stdout, "...Unable to read Tip for Device ID %d\n", pdtDeviceHandles[i].nID );
			}
			else
			{
				fprintf( stdout, "...Tip for Device ID %d = (%.3f, %.3f, %.3f) (diameter %.3f)\n", pdtDeviceHandles[i].nID, tip.x, tip.y, tip.z, tipDiameter );
			}

			// Add the rigid body from Device Handle
			if( RigidBodyAddFromDeviceHandle( pdtDeviceHandles[i].nID, totalRigids, 0 ) != OPTO_NO_ERROR_CODE )
			{
				goto ERROR_EXIT;
			} /* if */
			totalRigids++;
		}
	}

	fprintf( stdout, "\nRIGID BODIES:\n" );
	for( i = 0; i < totalRigids; i++ )
	{
		for( tipID = 1; tipID <= MAX_TIP_ID; tipID++ )
		{
			// Load the TIP file from Rigid Body
			if( OptotrakRigidBodyLoadTip( i, tipID, &tip, &tipDiameter ) == OPTO_OK )
			{
				fprintf( stdout, "...Found Tip for Rigid Body %d (Tip ID %d) = (%.3f, %.3f, %.3f) (diameter %.3f)\n", i, tipID, tip.x, tip.y, tip.z, tipDiameter );
			}
		}
	}

PROGRAM_COMPLETE:
    /*
     * CLEANUP
     */
	fprintf( stdout, "\n" );
	fprintf( stdout, "...TransputerShutdownSystem\n" );
    TransputerShutdownSystem( );

	/*
	 * free all memory
	 */
	if( pdtDeviceHandlesInfo )
	{
		for( i = 0; i < nDevices; i++ )
		{
			AllocateMemoryDeviceHandleProperties( &(pdtDeviceHandlesInfo[i].grProperties), 0 );
		} /* for */
	} /* if */
	AllocateMemoryDeviceHandles( &pdtDeviceHandles, 0 );
	AllocateMemoryDeviceHandlesInfo( &pdtDeviceHandlesInfo, pdtDeviceHandles, 0 );

	exit( 0 );

ERROR_EXIT:
	/*
	 * Indicate that an error has occurred
	 */
	fprintf( stdout, "\nAn error has occurred during execution of the program.\n" );
    if( OptotrakGetErrorString( szNDErrorString, MAX_ERROR_STRING_LENGTH + 1 ) == 0 )
    {
        fprintf( stdout, szNDErrorString );
    } /* if */

	fprintf( stdout, "\n\n...TransputerShutdownSystem\n" );
	OptotrakDeActivateMarkers( );
	TransputerShutdownSystem( );

	/*
	 * free all memory
	 */
	if( pdtDeviceHandlesInfo )
	{
		for( i = 0; i < nDevices; i++ )
		{
			AllocateMemoryDeviceHandleProperties( &(pdtDeviceHandlesInfo[i].grProperties), 0 );
		} /* for */
	} /* if */
	AllocateMemoryDeviceHandles( &pdtDeviceHandles, 0 );
	AllocateMemoryDeviceHandlesInfo( &pdtDeviceHandlesInfo, pdtDeviceHandles, 0 );

    exit( 1 );

} /* main */

